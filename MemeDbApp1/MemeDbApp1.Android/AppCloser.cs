﻿using MemeDbApp1.Utility;
using Plugin.CurrentActivity;

namespace MemeDbApp1.Droid
{
    public class AppCloser : IAppCloser
    {
        public void CloseApplication()
        {
            var activity = CrossCurrentActivity.Current.Activity;
            activity.FinishAffinity();
        }
    }
}