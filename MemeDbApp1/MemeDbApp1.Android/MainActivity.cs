﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using MemeDbApp1.Utility;
using MimeDetective;
using Plugin.Permissions;
using Prism;
using Prism.Ioc;
using System.IO;
using static Android.Content.ClipData;

namespace MemeDbApp1.Droid
{
    // NOTE: Future warning: Names of mipmap files CANNOT CONTAIN CAPITAL LETTERS.
    [Activity(Label = "MemeDbApp1", Icon = "@mipmap/tms_logo", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    [IntentFilter(new[] { Android.Content.Intent.ActionSend },
                  Categories = new[] { Android.Content.Intent.CategoryDefault },
                  DataMimeType = @"image/*")]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            Xamarin.Essentials.Platform.Init(this, bundle);
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, bundle);

            // Set platform-specific common filesystem paths.
            Utility.FsPaths.MainInternalDirectory = this.GetLocalStoragePath();
            Utility.FsPaths.MainDatabasePath = System.IO.Path.Combine(this.GetLocalStoragePath(), "Data.db");
            string packageName = Application.Context.PackageName;
            string imagesDirectory = System.IO.Path.Combine(this.GetRoamingStoragePath(), "Pictures", packageName);
            Utility.FsPaths.ImagesDirectory = imagesDirectory;
            string publicImagesDirectory = System.IO.Path.Combine(this.GetRoamingStoragePath());
            Utility.FsPaths.PublicImagesDirectory = publicImagesDirectory;

            var app = new App(new AndroidInitializer());
            LoadApplication(app);

            if (Intent.Action == Android.Content.Intent.ActionSend)
            {
                System.DateTime currentTime = System.DateTime.UtcNow;

                Item imageClipData = Intent.ClipData.GetItemAt(0);
                string mimeType = Intent.Type;
                System.IO.Stream imageStream = ContentResolver.OpenInputStream(imageClipData.Uri);
                var imgMemoryStream = new System.IO.MemoryStream();
                imageStream.CopyTo(imgMemoryStream);
                if (string.IsNullOrWhiteSpace(mimeType) || mimeType.EndsWith("*"))
                {
                    // The source did not give a specific MIME type (ie it passed "image/*"). Determine it from the file data.
                    string tempSavePath = Path.Combine(Utility.FsPaths.MainInternalDirectory, "tempImage");
                    File.WriteAllBytes(tempSavePath, imgMemoryStream.ToArray());
                    mimeType = new FileInfo(tempSavePath).GetFileType().Mime;
                    File.Delete(tempSavePath);
                }
                app.HandleImageAsync(imgMemoryStream, mimeType, currentTime);
            }
        }

        private string GetLocalStoragePath()
        {
            string localRoot = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return localRoot;
        }

        private string GetRoamingStoragePath()
        {
            string sdRoot = Android.OS.Environment.ExternalStorageDirectory.Path;
            return sdRoot;
        }
    }

    public class AndroidInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IAppCloser, AppCloser>();
            containerRegistry.Register<IPublicImageSaver, PublicImageSaver>();
        }
    }
}

