﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MemeDbApp1.Utility;
using Plugin.CurrentActivity;

namespace MemeDbApp1.Droid
{
    public class PublicImageSaver : IPublicImageSaver
    {
        private const string TEMP_FILE_FORMAT = "{0}.{1}";

        public void SaveToPublicDirectory(string originalPath, string tempName, string mimeType)
        {
            if (originalPath == null)
                { throw new ArgumentNullException(nameof(originalPath)); }

            if (string.IsNullOrWhiteSpace(originalPath))
                { throw new ArgumentException("The given path is not valid.", nameof(originalPath)); }

            if (tempName == null)
                { throw new ArgumentNullException(nameof(tempName)); }

            if (string.IsNullOrWhiteSpace(tempName))
                { throw new ArgumentException("The given temporary file name is not valid.", nameof(tempName)); }

            if (mimeType == null)
                { throw new ArgumentNullException(nameof(mimeType)); }

            if (string.IsNullOrWhiteSpace(mimeType) || !mimeType.StartsWith("image/"))
                { throw new ArgumentException("The given MIME type is not valid.", nameof(mimeType)); }

            string publicImagesDirectory = Utility.FsPaths.PublicImagesDirectory;

            if (string.IsNullOrWhiteSpace(publicImagesDirectory))
                { throw new InvalidOperationException("Set Utility.FsPaths.PublicImagesDirectory before calling this."); }
            
            string extension = MimeManager.MimeTypeToExtension(mimeType);
            string tempFilePath = Path.Combine(publicImagesDirectory, string.Format(TEMP_FILE_FORMAT, tempName, extension));

            // Copy image to temporary location.
            // Writes the file content manually to a new file, rather than copying. Might help with making sure the date created/modified are fully up to date.
            File.WriteAllBytes(tempFilePath, File.ReadAllBytes(originalPath));

            // Set last modified and last access times to now.
            // This makes sure that the picture shows up first in other apps' listings.
            DateTime now = DateTime.Now;
            File.SetCreationTime(tempFilePath, now);
            File.SetLastWriteTime(tempFilePath, now);
            File.SetLastAccessTime(tempFilePath, now);

            // Register image with Android, so apps will realize it's there.
            MediaScannerConnection.ScanFile(CrossCurrentActivity.Current.AppContext,
                                            new string[] { tempFilePath },
                                            new string[] { mimeType },
                                            null);
        }

        public void RemoveFromPublicDirectory(string tempName, string extension)
        {
            if (tempName == null)
                { throw new ArgumentNullException(nameof(tempName)); }

            if (string.IsNullOrWhiteSpace(tempName))
                { throw new ArgumentException("The given temporary file name is not valid.", nameof(tempName)); }

            if (extension == null)
                { throw new ArgumentNullException(nameof(extension)); }

            if (string.IsNullOrWhiteSpace(extension))
                { throw new ArgumentException("The given file extension is not valid.", nameof(extension)); }

            string publicImagesDirectory = Utility.FsPaths.PublicImagesDirectory;

            if (string.IsNullOrWhiteSpace(publicImagesDirectory))
                { throw new InvalidOperationException("Set Utility.FsPaths.PublicImagesDirectory before calling this."); }

            string tempFileName = string.Format(TEMP_FILE_FORMAT, tempName, extension);
            string tempFilePath = Path.Combine(publicImagesDirectory, tempFileName);

            // Unregister image with Android. Most apps will realize that it's no longer there after this, but some will have cached it.
            ContentResolver resolver = CrossCurrentActivity.Current.AppContext.ContentResolver;
            Android.Net.Uri rootUri = MediaStore.Video.Media.ExternalContentUri;
            string mediaQueryString = MediaStore.MediaColumns.Data + "=?";
            resolver.Delete(rootUri, mediaQueryString, new string[] { tempFilePath });

            // Delete image from temporary location.
            File.Delete(tempFilePath);

            // Unregisters the image with Android in a second way. I think both of these are necessary to reliably work.
            MediaScannerConnection.ScanFile(CrossCurrentActivity.Current.AppContext,
                                            new string[] { tempFilePath },
                                            null,
                                            null);
        }
    }
}