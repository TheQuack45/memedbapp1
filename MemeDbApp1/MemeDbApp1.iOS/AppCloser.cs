﻿using System.Threading;
using MemeDbApp1.Utility;

namespace MemeDbApp1.iOS
{
    public class AppCloser : IAppCloser
    {
        public void CloseApplication()
        {
            Thread.CurrentThread.Abort();
        }
    }
}