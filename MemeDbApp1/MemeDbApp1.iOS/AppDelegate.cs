﻿using Foundation;
using MemeDbApp1.Utility;
using Prism;
using Prism.Ioc;
using System;
using UIKit;

namespace MemeDbApp1.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        private App _mainApp;

        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            SQLitePCL.Batteries_V2.Init();

            global::Xamarin.Forms.Forms.Init();

            // Set platform-specific common filesystem paths.
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string imagesPath = System.IO.Path.Combine(documentsPath, "Images");
            string libraryPath = System.IO.Path.Combine(documentsPath, "..", "Library");
            string mainDatabasePath = System.IO.Path.Combine(libraryPath, "Data.db");
            Utility.FsPaths.ImagesDirectory = imagesPath;
            Utility.FsPaths.MainInternalDirectory = libraryPath;
            Utility.FsPaths.MainDatabasePath = mainDatabasePath;

            this._mainApp = new App(new iOSInitializer());
            LoadApplication(this._mainApp);

            return base.FinishedLaunching(app, options);
        }

        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            string stringUrl = url.Path;

            return true;
        }
    }

    public class iOSInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IPublicImageSaver, PublicImageSaver>();
        }
    }
}
