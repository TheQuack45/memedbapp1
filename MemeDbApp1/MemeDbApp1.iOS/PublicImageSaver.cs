﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using MemeDbApp1.Utility;
using UIKit;

namespace MemeDbApp1.iOS
{
    public class PublicImageSaver : IPublicImageSaver
    {
        private UIImage _image;

        public void SaveToPublicDirectory(string originalPath, string tempName, string mimeType)
        {
            if (originalPath == null)
                { throw new ArgumentNullException(nameof(originalPath)); }

            if (string.IsNullOrWhiteSpace(originalPath))
                { throw new ArgumentException("The given path is not valid.", nameof(originalPath)); }

            this._image = new UIImage(originalPath);

            UIApplication.SharedApplication.InvokeOnMainThread(this.Save);
        }

        public void RemoveFromPublicDirectory(string tempName, string extension)
        {
            throw new NotSupportedException("Images cannot be removed from the photo album on iOS.");
        }

        private void Save()
        {
            if (this._image == null)
                { throw new InvalidOperationException("_image cannot be null."); }

            this._image.SaveToPhotosAlbum(null);
        }
    }
}