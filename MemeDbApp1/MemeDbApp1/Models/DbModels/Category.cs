﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MemeDbApp1.Models
{
    [Table(nameof(Category))]
    public class Category
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid? ParentId { get; set; }
        public Category Parent { get; set; }
        public ICollection<Category> Children { get; set; } = new List<Category>();

        public ICollection<MemeCategory> MemeCategory { get; set; } = new List<MemeCategory>();
    }
}
