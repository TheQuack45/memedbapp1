﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MemeDbApp1.Models
{
    [Table(nameof(Meme))]
    public class Meme
    {
        public Guid Id { get; set; }

        // TODO: Should SortableAttribute be applied on a ViewModel class?
        [Sortable(nameof(Caption))]
        public string Caption { get; set; }
        public string MimeType { get; set; }
        // This is nullable, because SQLite doesn't understand how to assign a default value of "literally right now". But it should never be left unset.
        [Sortable(nameof(DateAdded), DisplayName = "Date")]
        public DateTime? DateAdded { get; set; }

        public ICollection<MemeCategory> MemeCategory { get; set; } = new List<MemeCategory>();

        public string Path
        {
            get
            {
                // TODO: This is business logic. That's a sign that this needs to move into the ViewModel.
                string originalPath = System.IO.Path.Combine(Utility.FsPaths.ImagesDirectory, (this.Id + "." + Utility.MimeManager.MimeTypeToExtension(this.MimeType)));
                if (System.IO.File.Exists(originalPath))
                    { return originalPath; }
                else
                    { return "NotFound"; }
            }
        }
    }
}
