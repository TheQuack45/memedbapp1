﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MemeDbApp1.Models
{
    [Table(nameof(MemeCategory))]
    public class MemeCategory
    {
        [ForeignKey(nameof(Models.Meme))]
        public Guid MemeId { get; set; }
        public Meme Meme { get; set; }

        [ForeignKey(nameof(Models.Category))]
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
