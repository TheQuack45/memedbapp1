﻿using System;

namespace MemeDbApp1.Models
{
    /// <summary>
    /// Represents an attribute that marks a model property as being valid to sort by.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property,
                    AllowMultiple = false,
                    Inherited = true)]
    public sealed class SortableAttribute : Attribute
    {
        /// <summary>
        /// A unique name for this property. Should match the identifier for the property in the model class.
        /// </summary>
        public string Key { get; }

        private string _displayName;
        /// <summary>
        /// A user-readable name for this property.
        /// </summary>
        public string DisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(this._displayName))
                    { return this.Key; }
                return this._displayName;
            }
            set => this._displayName = value;
        }

        /// <summary>
        /// Marks this property as being valid for use in sorting.
        /// </summary>
        /// <param name="key">A unique name for this property. Should match the identifier for the property in the model class.</param>
        public SortableAttribute(string key)
        {
            this.Key = key;
        }
    }
}
