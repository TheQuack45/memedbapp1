﻿using System;

namespace MemeDbApp1.Models
{
    public class MainPageMenuItem
    {
        public string Title { get; set; }
        public string IconSource { get; set; }
        public Type TargetType { get; set; }
    }
}
