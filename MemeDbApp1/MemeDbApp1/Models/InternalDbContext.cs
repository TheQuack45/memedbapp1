﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MemeDbApp1.Models
{
    public class InternalDbContext : DbContext
    {
        private const long DB_USER_VERSION = 2;

        private string _databaseFilePath;

        public DbSet<Meme> Meme { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<MemeCategory> MemeCategory { get; set; }

        public InternalDbContext(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            else if (path == String.Empty)
            {
                throw new ArgumentException("The given path must not be empty.", nameof(path));
            }

            this._databaseFilePath = path;

            //Database.EnsureDeleted();
            Database.Migrate();

            Database.ExecuteSqlCommand(new RawSqlString($"PRAGMA user_version = {DB_USER_VERSION}"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MemeCategory>()
                        .HasKey(mc => new { mc.MemeId, mc.CategoryId });
            modelBuilder.Entity<MemeCategory>()
                        .HasOne(mc => mc.Meme)
                        .WithMany(meme => meme.MemeCategory)
                        .HasForeignKey(mc => mc.MemeId);
            modelBuilder.Entity<MemeCategory>()
                        .HasOne(mc => mc.Category)
                        .WithMany(c => c.MemeCategory)
                        .HasForeignKey(mc => mc.CategoryId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (this._databaseFilePath == null)
            {
                throw new InvalidOperationException("This database must have a valid path set.");
            }

            optionsBuilder.UseSqlite($"Data Source={this._databaseFilePath}");
        }
    }
}
