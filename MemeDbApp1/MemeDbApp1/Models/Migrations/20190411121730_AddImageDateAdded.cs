﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MemeDbApp1.Models.Migrations
{
    public partial class AddImageDateAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateAdded",
                table: "Meme",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateAdded",
                table: "Meme");
        }
    }
}
