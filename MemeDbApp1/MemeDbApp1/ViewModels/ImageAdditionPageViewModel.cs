﻿using MemeDbApp1.Models;
using MemeDbApp1.Utility;
using Microsoft.EntityFrameworkCore;
using MimeDetective;
using Plugin.Permissions;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Xamarin.Forms;

namespace MemeDbApp1.ViewModels
{
    public class ImageAdditionPageViewModel : ViewModelBase, INavigationAware
    {
        #region Members
        #region Static members
        private const string DIALOG_ERROR_TITLE = "Failed to Save";
        private const string DIALOG_OK = "Ok";
        private const string DIALOG_ERROR_PROMPT_TEMPLATE = "Failed to save your meme. Please try again. If the issue continues, contact support and send them this error message: {0}";

        private const string DIALOG_NO_PATH_PROMPT = "Did not receive a path to the image.";
        private const string DIALOG_INVALID_PATH_PROMPT = "An image was not found at the provided path.";
        private const string DIALOG_INVALID_MIME_PROMPT = "An invalid MIME type was provided.";
        #endregion Static members

        #region Command members
        private Command _saveImageCommand;
        public Command SaveImageCommand
        {
            get
            {
                if (this._saveImageCommand == null)
                {
                    this._saveImageCommand = new Command(() => this.SaveImageAsync());
                }
                return this._saveImageCommand;
            }
        }

        private Command _cancelCommand;
        public Command CancelCommand
        {
            get
            {
                if (this._cancelCommand == null)
                {
                    this._cancelCommand = new Command(() => this.CancelSaveAsync());
                }
                return this._cancelCommand;
            }
        }

        private Command _selectCategoriesCommand;
        public Command SelectCategoriesCommand
        {
            get
            {
                if (this._selectCategoriesCommand == null)
                { this._selectCategoriesCommand = new Command(this.OnSelectCategories); }
                return this._selectCategoriesCommand;
            }
        }
        #endregion Command members

        #region Properties
        private ImageSource _image;
        public ImageSource Image
        {
            get { return this._image; }
            set { this.SetProperty(ref this._image, value, "Image"); }
        }

        private string _caption;
        public string Caption
        {
            get { return this._caption; }
            set { this.SetProperty(ref this._caption, value, "Caption"); }
        }

        public DateTime _timeAdded;
        /// <summary>
        /// Represents the time that this picture was selected to add. Should be in LOCAL TIME, not UTC.
        /// </summary>
        public DateTime TimeAdded
        {
            get => this._timeAdded;
            set => this.SetProperty(ref this._timeAdded, value, nameof(TimeAdded));
        }

        private List<Guid> _imageCategoryIds;
        public List<Guid> ImageCategoryIds
        {
            get => this._imageCategoryIds;
            set => this.SetProperty(ref this._imageCategoryIds, value, nameof(ImageCategoryIds));
        }
        #endregion Properties

        #region Fields
        private IPageDialogService _dialogService;
        private string _imagePath;
        private string _imageMimeType;
        #endregion Fields
        #endregion Members

        #region Constructors
        public ImageAdditionPageViewModel(INavigationService navService, IPageDialogService dialogService)
            : base(navService)
        {
            this.Title = "Add Meme";
            this._dialogService = dialogService;
        }
        #endregion Constructors

        #region Methods
	    #region Command handlers
        private async void CancelSaveAsync()
        {
            await this.NavigationService.GoBackAsync();
        }

        private async void SaveImageAsync()
        {
            string caption = this.Caption;
            if (string.IsNullOrWhiteSpace(caption))
            {
                // The caption is either 1) Empty or 2) Entirely whitespace. This means the user didn't want to specify a caption.
                caption = null;
            }

            DateTime timeAdded = this.TimeAdded;
            DateTime timeAddedUtc = timeAdded.ToUniversalTime();

            await CrossPermissions.Current.RequestPermissionsAsync(new[] { Plugin.Permissions.Abstractions.Permission.Storage });

            string extension = MimeManager.MimeTypeToExtension(this._imageMimeType);
            if (string.IsNullOrWhiteSpace(extension))
            {
                Debug.WriteLine("{0}.{1}() could not find a valid extension for the MIME type {2}. Image saving failed.",
                                nameof(ImageAdditionPageViewModel),
                                Utility.MethodName.Get(),
                                this._imageMimeType);
                await this._dialogService.DisplayAlertAsync(DIALOG_ERROR_TITLE,
                                                            string.Format(DIALOG_ERROR_PROMPT_TEMPLATE, DIALOG_INVALID_MIME_PROMPT),
                                                            DIALOG_OK);
                await this.NavigationService.GoBackAsync();
                return;
            }

            Guid imageId = this.GenerateUniqueImageId();
            string imageFileName = String.Format("{0}.{1}",
                                                 imageId.ToString(),
                                                 extension);
            string imageSavePath = System.IO.Path.Combine(Utility.FsPaths.ImagesDirectory, imageFileName);
            
            try
            {
                System.IO.File.Move(this._imagePath, imageSavePath);
            }
            catch (System.IO.IOException e)
            {
                string error = $"{nameof(System.IO.IOException)} in {nameof(ImageAdditionPageViewModel)}.{Utility.MethodName.Get()}.";
                Debug.WriteLine(error + " Message: {1}", e.Message);
                await this._dialogService.DisplayAlertAsync(DIALOG_ERROR_TITLE,
                                                            string.Format(DIALOG_ERROR_PROMPT_TEMPLATE, error),
                                                            DIALOG_OK);
                await this.NavigationService.GoBackAsync();
                return;
            }
            catch (UnauthorizedAccessException e)
            {
                string error = $"{nameof(UnauthorizedAccessException)} in {nameof(ImageAdditionPageViewModel)}.{Utility.MethodName.Get()}.";
                Debug.WriteLine(error + " Message: {1}", e.Message);
                await this._dialogService.DisplayAlertAsync(DIALOG_ERROR_TITLE,
                                                            string.Format(DIALOG_ERROR_PROMPT_TEMPLATE, error),
                                                            DIALOG_OK);
                await this.NavigationService.GoBackAsync();
                return;
            }
            catch (Exception e)
            {
                string error = $"General {nameof(Exception)} in {nameof(ImageAdditionPageViewModel)}.{Utility.MethodName.Get()}.";
                Debug.WriteLine(error + " Message: {1}", e.Message);
                await this._dialogService.DisplayAlertAsync(DIALOG_ERROR_TITLE,
                                                            string.Format(DIALOG_ERROR_PROMPT_TEMPLATE, error),
                                                            DIALOG_OK);
                await this.NavigationService.GoBackAsync();
                return;
            }

            try
            {
                this.AddImageToDatabase(imageId, this._imageMimeType, caption, timeAddedUtc);
            }
            catch (DbUpdateConcurrencyException e)
            {
                string error = $"{nameof(DbUpdateConcurrencyException)} in {nameof(ImageAdditionPageViewModel)}.{Utility.MethodName.Get()}.";
                Debug.WriteLine(error + " Message: {1}", e.Message);
                await this._dialogService.DisplayAlertAsync(DIALOG_ERROR_TITLE,
                                                            string.Format(DIALOG_ERROR_PROMPT_TEMPLATE, error),
                                                            DIALOG_OK);
                await this.NavigationService.GoBackAsync();
                return;
            }
            catch (DbUpdateException e)
            {
                string error = $"General {nameof(DbUpdateException)} in {nameof(ImageAdditionPageViewModel)}.{Utility.MethodName.Get()}.";
                Debug.WriteLine(error + " Message: {1}", e.Message);
                await this._dialogService.DisplayAlertAsync(DIALOG_ERROR_TITLE,
                                                            string.Format(DIALOG_ERROR_PROMPT_TEMPLATE, error),
                                                            DIALOG_OK);
                await this.NavigationService.GoBackAsync();
                return;
            }

            // Navigates to a new details page for this meme.
            INavigationParameters navParams = new NavigationParameters();
            navParams.Add("Id", imageId);
            await this.NavigationService.NavigateAsync(Utility.NavPath.Get(true, nameof(MainPageViewModel), nameof(NavigationPage), nameof(ImageListPageViewModel), nameof(ImageDetailsPageViewModel)), navParams);
        }

	    private void OnSelectCategories()
        {
            IEnumerable<Guid> categoryIds = this.ImageCategoryIds;

            var navParams = new NavigationParameters();
            navParams.Add("CategoryIds", categoryIds);
            this.NavigationService.NavigateAsync(Utility.NavPath.Get(nameof(CategorySelectionPageViewModel)), navParams);
        }
        #endregion Command

        #region Event handlers
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.TryGetValue("ImagePath", out string imagePath) &&
                parameters.TryGetValue("ImageMimeType", out string mimeType) &&
                parameters.TryGetValue("TimeAddedUtc", out DateTime timeAddedUtc))
            {
                // Initial navigation, from the Share intent. Load the image details.
                this.LoadImageDetails(imagePath, mimeType, timeAddedUtc);
            }
            else if (parameters.TryGetValue("CategoryIds", out List<Guid> categoryIds))
            {
                // Navigated back to from CategorySelectionPage. Update the categories.
                if (categoryIds == null)
                {
                    // The user pressed cancel. Change nothing.
                    return;
                }
                this.UpdateCategoryIds(categoryIds);
            }
            else
            {
                // Invalid scenario. Go back.
                Debug.WriteLine("An instance of {0} received no parameters during navigation. This is an invalid scenario. Going back.", new string[] { nameof(ImageAdditionPageViewModel) });
                this.NavigationService.GoBackAsync();
            }
        }
        #endregion Event handlers

        private void UpdateCategoryIds(List<Guid> categoryIds)
        {
            this.ImageCategoryIds = categoryIds;
        }

        private async void LoadImageDetails(string imagePath, string mimeType, DateTime timeAddedUtc)
        {
            if (string.IsNullOrWhiteSpace(imagePath))
            {
                // There was no image path provided.
                Debug.WriteLine("No image path was provided to an instance of {0} during navigation.", new string[] { nameof(ImageAdditionPageViewModel) });
                await this._dialogService.DisplayAlertAsync(DIALOG_ERROR_TITLE,
                                                            string.Format(DIALOG_ERROR_PROMPT_TEMPLATE, DIALOG_NO_PATH_PROMPT),
                                                            DIALOG_OK);
                await this.NavigationService.GoBackAsync();
                return;
            }

            if (!System.IO.File.Exists(imagePath))
            {
                // The given path is invalid or the image does not exist.
                Debug.WriteLine("An invalid or nonexistent path to an image was provided to an instance of {0} during navigation.", new string[] { nameof(ImageAdditionPageViewModel) });
                await this._dialogService.DisplayAlertAsync(DIALOG_ERROR_TITLE,
                                                            string.Format(DIALOG_ERROR_PROMPT_TEMPLATE, DIALOG_INVALID_PATH_PROMPT),
                                                            DIALOG_OK);
                await this.NavigationService.GoBackAsync();
                return;
            }

            if (!(new FileInfo(imagePath)).GetFileType().Mime.StartsWith("image/"))
            {
                // The given path does not point to an image.
                Debug.WriteLine("An invalid or nonexistent path to an image was provided to an instance of {0} during navigation.", new string[] { nameof(ImageAdditionPageViewModel) });
                await this._dialogService.DisplayAlertAsync(DIALOG_ERROR_TITLE,
                                                            string.Format(DIALOG_ERROR_PROMPT_TEMPLATE, DIALOG_INVALID_PATH_PROMPT),
                                                            DIALOG_OK);
                await this.NavigationService.GoBackAsync();
                return;
            }

            if (string.IsNullOrWhiteSpace(mimeType))
            {
                // The given path is invalid or the image does not exist.
                Debug.WriteLine("An invalid MIME type was provided to an instance of {0} during navigation.", new string[] { nameof(ImageAdditionPageViewModel) });
                await this._dialogService.DisplayAlertAsync(DIALOG_ERROR_TITLE,
                                                            string.Format(DIALOG_ERROR_PROMPT_TEMPLATE, DIALOG_INVALID_MIME_PROMPT),
                                                            DIALOG_OK);
                await this.NavigationService.GoBackAsync();
                return;
            }

            this.Image = ImageSource.FromFile(imagePath);
            this.TimeAdded = timeAddedUtc.ToLocalTime();
            this._imagePath = imagePath;
            this._imageMimeType = mimeType;
            this.ImageCategoryIds = new List<Guid>();
        }

        /// <summary>
        /// Generates a unique image ID that is not present in the database.
        /// </summary>
        /// <returns>System.Guid that is a new ID.</returns>
        private Guid GenerateUniqueImageId()
        {
            Guid imageId;
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                do
                {
                    imageId = Guid.NewGuid();
                } while (dbContext.Meme.Any(image => image.Id == imageId));
            }
            return imageId;
        }

        /// <summary>
        /// Adds a new image to the database with the given information. Generates a unique ID automatically.
        /// </summary>
        /// <param name="mimeType">System.String that is the MIME type of this image.</param>
        /// <param name="caption">System.String that is the caption for the image. If null, it will be stored as string.Empty.</param>
        /// <param name="timeAddedUniversal">System.DateTime that is the time the image was added at. Should be in UTC, NOT local time.</param>
        /// <exception cref="ArgumentNullException">Thrown if `mimeType` is null.</exception>
        /// <exception cref="ArgumentException">Thrown if `mimeType` is empty or not an image type.</exception>
        private void AddImageToDatabase(string mimeType, string caption, DateTime timeAddedUniversal)
        {
            // Generates a unique ID.
            Guid imageId = this.GenerateUniqueImageId();

            this.AddImageToDatabase(imageId, mimeType, caption, timeAddedUniversal);
        }

        /// <summary>
        /// Adds a new image to the database with the given information.
        /// </summary>
        /// <param name="id">System.Guid that is the ID of this image.</param>
        /// <param name="mimeType">System.String that is the MIME type of this image.</param>
        /// <param name="caption">System.String that is the caption for the image. If null, it will be stored as string.Empty.</param>
        /// <param name="timeAddedUniversal">System.DateTime that is the time the image was added at. Should be in UTC, NOT local time.</param>
        /// <exception cref="ArgumentNullException">Thrown if `mimeType` is null.</exception>
        /// <exception cref="ArgumentException">Thrown if `id` is empty or if `mimeType` is empty or not an image type.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the given ID is already in the database.</exception>
        private void AddImageToDatabase(Guid id, string mimeType, string caption, DateTime timeAddedUniversal)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentException("The given ID cannot be empty.", nameof(id));
            }

            if (mimeType == null)
            {
                throw new ArgumentNullException(nameof(mimeType));
            }

            if (string.IsNullOrWhiteSpace(mimeType) || !mimeType.StartsWith("image/"))
            {
                throw new ArgumentException("The given MIME type must be a valid image type.", nameof(mimeType));
            }

            if (string.IsNullOrWhiteSpace(caption))
            {
                caption = string.Empty;
            }

            var newImage = new Models.Meme()
            {
                Id = id,
                MimeType = mimeType,
                Caption = caption,
                DateAdded = timeAddedUniversal,
            };
            IEnumerable<Guid> categoryIds = this.ImageCategoryIds;

            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.Meme.Any(image => image.Id == id))
                {
                    throw new InvalidOperationException("There is already an image with the given ID in the database.");
                }

                // Add image to database.
                dbContext.Meme.Add(newImage);

                // Add all of the selected categories to the database.
                foreach (Guid categoryId in categoryIds)
                {
                    Category category = dbContext.Category.Single(c => c.Id == categoryId);
                    dbContext.MemeCategory.Add(new MemeCategory() { Meme = newImage,
                                                                        Category = category });
                }

                dbContext.SaveChanges();
            }
        }
        #endregion Methods
    }
}
