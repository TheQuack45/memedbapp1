﻿using MemeDbApp1.Models;
using Microsoft.EntityFrameworkCore;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace MemeDbApp1.ViewModels
{
    public class CategoryDetailsPageViewModel : ViewModelBase
    {
        #region Members
        #region Static members
        // TODO: These should be localizable strings.
        private const string DELETE_DIALOG_PROMPT = "Are you sure you want to delete this category?";
        private const string DELETE_DIALOG_CANCEL = "No, cancel";
        private const string DELETE_DIALOG_YES = "Yes, I'm sure";

        private const string INVALID_STATE_TITLE = "Invalid State";
        private const string INVALID_STATE_TEXT = "This page's state is invalid. Please try whatever you were doing again, and if the issue continues please contact support.";

        private const string CANNOT_DELETE_TITLE = "Cannot Delete";
        private const string OK_TEXT = "OK";

        private const string CHILDREN_DELETE_CONFLICT_ALERT_TEXT = "This category cannot be deleted while it has other categories as its parent. Please delete those categories before deleting this one.";

        private const string PARENT_SELF_TEXT = "You cannot set a category as its own parent.";
        private const string PARENT_CHILD_TEXT = "A category's child cannot be its parent.";

        private const string PARENT_FAIL_TITLE = "Update Failed";
        private const string PARENT_FAIL_TEXT = "Failed to update the parent category. Please try again, and if the issue continues please contact support.";
        #endregion Static members

        public bool IsNameInvalid
        {
            get => string.IsNullOrWhiteSpace(this.CategoryName);
        }

        private Category _category;
        public Category Category
        {
            get => this._category;
            set
            {
                this.SetProperty(ref this._category, value, nameof(Category));
                this.RaisePropertyChanged(nameof(CategoryName));
                this.RaisePropertyChanged(nameof(IsNameInvalid));
                this.RaisePropertyChanged(nameof(SelectParentButtonText));
            }
        }

        public string CategoryName
        {
            get
            {
                if (this.Category == null)
                    { return null; }
                else
                    { return this.Category.Name; }
            }
            set
            {
                if (this.Category == null)
                    { return; }

                this.Category.Name = value;
                this.RaisePropertyChanged(nameof(CategoryName));
                this.RaisePropertyChanged(nameof(IsNameInvalid));
            }
        }

        public string SelectParentButtonText
        {
            get
            {
                if (this.Category == null)
                    { return null; }

                string parentName = this.Category.Parent == null ? "None"
                                                                 : this.Category.Parent.Name;
                return string.Format("Parent: {0}", parentName);
            }
        }

        #region Fields
        private bool _isDeleted;
        private IPageDialogService _dialogService;
        #endregion Fields

        #region Command members
        private Command _updateNameCommand;
        public Command UpdateNameCommand
        {
            get
            {
                if (this._updateNameCommand == null)
                    { this._updateNameCommand = new Command(this.OnUpdateNameAsync); }
                return this._updateNameCommand;
            }
        }

        private Command _selectParentCategoryCommand;
        public Command SelectParentCategoryCommand
        {
            get
            {
                if (this._selectParentCategoryCommand == null)
                    { this._selectParentCategoryCommand = new Command(this.OnSelectParentCategory); }
                return this._selectParentCategoryCommand;
            }
        }

        private Command _deleteCommand;
        public Command DeleteCommand
        {
            get
            {
                if (this._deleteCommand == null)
                    { this._deleteCommand = new Command(this.OnDeleteCategory); }
                return this._deleteCommand;
            }
        }
        #endregion Command members
        #endregion Members

        #region Constructors
        public CategoryDetailsPageViewModel(INavigationService navService, IPageDialogService dialogService)
            : base(navService)
        {
            this.Title = "Edit Category";
            this._isDeleted = false;
            this._dialogService = dialogService;
        }
        #endregion Constructors

        #region Methods
        private async void UpdateParentCategory(Category selectedParent)
        {
            if (this.Category == null)
            {
                // Category is invalid.
                Debug.WriteLine("{0}.{1} was null when {2}() tried to access it. This is invalid.",
                                nameof(CategoryDetailsPageViewModel),
                                nameof(this.Category),
                                Utility.MethodName.Get());
                await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                            INVALID_STATE_TEXT,
                                                            OK_TEXT);
                _ = await this.NavigationService.GoBackAsync();
                return;
            }

            if (selectedParent != null)
            {
                if (this.Category.Id == selectedParent.Id)
                {
                    // User cannot select a category as its own parent.
                    Debug.WriteLine("{0}.{1}() attempted to set a category as its own parent. Invalid task aborted.",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get());
                    await this._dialogService.DisplayAlertAsync(CANNOT_DELETE_TITLE,
                                                                PARENT_SELF_TEXT,
                                                                OK_TEXT);
                    return;
                }

                if (this.Category.Children.Select(c => c.Id).Contains(selectedParent.Id))
                {
                    // User cannot select a parent's child as its parent.
                    Debug.WriteLine("{0}.{1}() attempted to set a category's child as its parent. Invalid task aborted.",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get());
                    await this._dialogService.DisplayAlertAsync(CANNOT_DELETE_TITLE,
                                                                PARENT_CHILD_TEXT,
                                                                OK_TEXT);
                    return;
                }
            }

            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.Category == null)
                {
                    Debug.WriteLine("dbContext.Category was null when {0}.{1}() tried to access it. Category parent update failed.",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get());
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    return;
                }

                // Make sure you use the parent Category as retrieved directly from the database. At some point, the parameter 'parent' becomes a different object from the one in the database, but still has the same ID.
                // This results in a UNIQUE constraint failure if trying to assign 'parent' as the parent of any Category.

                Category currentCategory = dbContext.Category.Single(c => c.Id == this.Category.Id);
                var categoryTracker = dbContext.Category.Update(currentCategory);

                Category newParent;
                Guid? newParentId;
                if (selectedParent != null)
                {
                    if (categoryTracker.Entity.Parent != null && selectedParent.Id == categoryTracker.Entity.Parent.Id)
                    {
                        // If the new parent is the same as the current parent, do nothing.
                        return;
                    }

                    newParent = dbContext.Category.Single(c => c.Id == selectedParent.Id);
                    newParentId = newParent.Id;
                }
                else
                {
                    // The category should have no parent. Assign both values to null.
                    newParent = null;
                    newParentId = null;
                }

                // It seems like both of these properties need to be set for the relationship to be updated reliably.
                this.Category.Parent = newParent;
                categoryTracker.Entity.Parent = newParent;
                this.Category.ParentId = newParentId;
                categoryTracker.Entity.ParentId = newParentId;

                try
                {
                    dbContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException e)
                {
                    Debug.WriteLine("DbUpdateConcurrencyException in {0}.{1}(). Message: {2}",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    e.Message);
                    await this._dialogService.DisplayAlertAsync(PARENT_FAIL_TITLE,
                                                                PARENT_FAIL_TEXT,
                                                                OK_TEXT);
                    return;
                }
                catch (DbUpdateException e)
                {
                    Debug.WriteLine("DbUpdateException in {0}.{1}(). Message: {2}",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    e.Message);
                    await this._dialogService.DisplayAlertAsync(PARENT_FAIL_TITLE,
                                                                PARENT_FAIL_TEXT,
                                                                OK_TEXT);
                    return;
                }
            }

            this.RaisePropertyChanged(nameof(this.SelectParentButtonText));
        }

        /// <summary>
        /// Populates local data values (_parentCategory, CategoryName) from the database, for the category with the currently stored ID (CategoryId).
        /// </summary>
        /// <returns>True if successful, false if not.</returns>
        private bool PopulateFromDatabase(Guid categoryId)
        {
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                try
                {
                    // DO NOT USE METHOD A. It causes an EXTREMELY weird bug where category.ParentId will be populated properly, BUT category.Parent and category.Children WILL NOT BE; at first.
                    // If you wait some amount of time AFTER retrieving from the database they will THEN be populated.
                    // I do not understand it and so I must place a severe quarantine on this line, but I leave it here to remember: DON'T DEAD OPEN INSIDE.
                    // METHOD A. DO NOT UNCOMMENT THIS LINE.
                    ///////*var category = dbContext.Category.Single(current => current.Id == categoryId);*/
                    // METHOD B. USE THIS ONE.
                    List<Category> categories = dbContext.Category.ToList();
                    Category category = categories.Single(current => current.Id == categoryId);
                    // Stop scrolling, marine! Sorry for the scare, sir. Just making sure you're using method B and not method A.

                    this.Category = category;
                    this.RaisePropertyChanged(nameof(this.SelectParentButtonText));
                    // Are you sure you're not using method A?
                }
                catch (InvalidOperationException)
                {
                    Debug.WriteLine("{0}.{1}() found more than one category in InternalDbContext.Category that had the ID {2}. Retrieval cannot continue.",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    categoryId.ToString());
                    return false;
                }
            }

            return true;
        }

        #region Command handlers
        private async void OnDeleteCategory()
        {
            Guid categoryId = this.Category.Id;

            if (categoryId == null || categoryId == Guid.Empty)
            {
                // The category ID is invalid. How did this happen?
                Debug.WriteLine("{0}.{1}() found that the stored category ID is invalid. Deletion cannot continue.",
                                nameof(CategoryDetailsPageViewModel),
                                Utility.MethodName.Get());
                await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                            INVALID_STATE_TEXT,
                                                            OK_TEXT);
                _ = await this.NavigationService.GoBackAsync();
                return;
            }

            // Display a dialog to confirm that the user actually wants to delete the category.
            string userConfirmationChoice = await this._dialogService.DisplayActionSheetAsync(DELETE_DIALOG_PROMPT,
                                                                                              DELETE_DIALOG_CANCEL,
                                                                                              null, // I don't use the destructive option in ImageDetailsPage, so I didn't use it here so as to keep it consistent.
                                                                                              DELETE_DIALOG_YES);
            // If the user taps outside of the dialog, 'userConfirmationChoice' will be null. This should be considered a choice of "cancel".
            if (userConfirmationChoice == null || userConfirmationChoice == DELETE_DIALOG_CANCEL)
                { return; }

            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.Category == null)
                {
                    Debug.WriteLine("dbContext.Category was null when {0}.{1}() tried to access it. Category deletion failed.",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get());
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    _ = await this.NavigationService.GoBackAsync();
                    return;
                }

                // Select the right Category from the database.
                // I use this method instead of dbContext.Category.Single(predicate) because the latter method gets complex and ugly; a little moreso than this.
                IQueryable<Category> selectedCategoryList = dbContext.Category.Where(category => category.Id == categoryId);
                if (selectedCategoryList.Count() != 1)
                {
                    // There is either 0 or more than 1 meme in the database with the ID given.
                    Debug.WriteLine("{0}.{1}() found either 0 or >1 category in the database with the ID {2}. Category deletion failed.",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    categoryId.ToString());
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    _ = await this.NavigationService.GoBackAsync();
                    return;
                }
                Category selectedCategory = selectedCategoryList.Single<Category>();

                // Once again, we filter on Category.ToList() rather than directly on Category because it avoids that weird bug where Parent and Children are not populated.
                if (dbContext.Category.ToList().Single(category => category.Id == categoryId).Children.Count != 0)
                {
                    // This category has children. We cannot delete it right now.
                    // TODO: Allow the user to choose to delete the children as well as this category.
                    await this._dialogService.DisplayAlertAsync(CANNOT_DELETE_TITLE,
                                                                CHILDREN_DELETE_CONFLICT_ALERT_TEXT,
                                                                OK_TEXT);
                    return;
                }

                // Remove the category from the database.
                dbContext.Category.Remove(selectedCategory);

                // Remove all meme pairings with this category.
                dbContext.MemeCategory.RemoveRange(dbContext.MemeCategory.Where(ic => ic.CategoryId == selectedCategory.Id));

                try
                {
                    dbContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException e)
                {
                    Debug.WriteLine("DbUpdateConcurrencyException in {0}.{1}(). Message: {2}",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    e.Message);
                    return;
                }
                catch (DbUpdateException e)
                {
                    Debug.WriteLine("DbUpdateException in {0}.{1}(). Message: {2}",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    e.Message);
                    return;
                }
            }

            await this.NavigationService.GoBackAsync();
        }

        private async void OnUpdateNameAsync()
        {
            if (this._isDeleted)
                { return; }

            string name = this.Category.Name;
            if (string.IsNullOrWhiteSpace(name))
            {
                // The current name entered is invalid. Do nothing.
                // The user will be notified by the label that tells them to enter a name.
                return;
            }

            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.Category == null)
                {
                    Debug.WriteLine("dbContext.Category was null when {0}.{1}() tried to access it. Name update failed.",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get());
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    _ = await this.NavigationService.GoBackAsync();
                    return;
                }

                // Select the right Category from the database.
                // I use this method instead of dbContext.Meme.Single(predicate) because the latter method gets complex and ugly; a little moreso than this.
                IQueryable<Category> selectedCategoryList = dbContext.Category.Where(category => category.Id == this.Category.Id);
                if (selectedCategoryList.Count() != 1)
                {
                    // There is either 0 or more than 1 meme in the database with the ID given.
                    Debug.WriteLine("{0}.{1}() found either 0 or >1 category in the database with the ID {2}. Name update failed.",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    this.Category.Id.ToString());
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    _ = await this.NavigationService.GoBackAsync();
                    return;
                }
                Category selectedCategory = selectedCategoryList.Single<Category>();

                // Update the selected category with the new name. This change is not applied until dbContext.SaveChanges() is called.
                dbContext.Category.Update(selectedCategory).Entity.Name = name;
                try
                {
                    dbContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException e)
                {
                    Debug.WriteLine("DbUpdateConcurrencyException in {0}.{1}(). Message: {2}",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    e.Message);
                }
                catch (DbUpdateException e)
                {
                    Debug.WriteLine("DbUpdateException in {0}.{1}(). Message: {2}",
                                    nameof(CategoryDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    e.Message);
                }
            }
        }

        private async void OnSelectParentCategory()
        {
            // We include this category so that it and its children aren't displayed in the list of options.
            var navParams = new NavigationParameters();
            navParams.Add("ChildCategory", this.Category);
            await this.NavigationService.NavigateAsync(Utility.NavPath.Get(nameof(SelectParentCategoryPageViewModel)), navParams);
        }
        #endregion Command handlers

        public override async void OnNavigatedTo(INavigationParameters navParams)
        {
            if (navParams.TryGetValue("CategoryId", out Guid categoryId))
            {
                // Category ID was specified. This was navigated to by EditCategoriesPage.
                if (categoryId == null || categoryId == Guid.Empty)
                {
                    Debug.WriteLine("An instance of {0} received an invalid category ID. Navigation aborted.", new string[] { nameof(CategoryDetailsPageViewModel) });
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    _ = await this.NavigationService.GoBackAsync();
                    return;
                }

                if (!this.PopulateFromDatabase(categoryId))
                {
                    // Population from the database was unsuccessful.
                    Debug.WriteLine("An instance of {0} was unsuccessful when retrieving data from the database. Navigation aborted.", new string[] { nameof(CategoryDetailsPageViewModel) });
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    _ = await this.NavigationService.GoBackAsync();
                    return;
                }
            }
            else if (navParams.TryGetValue("SelectedCategory", out Category parentCategory))
            {
                // Parent category was specified. This was navigated back to from SelectParentCategoryPage.
                this.UpdateParentCategory(parentCategory);
            }
            else if (this.Category != null)
            {
                // This was navigated back to by SelectParentCategoryPage, but the user pressed cancel so they didn't choose a category.
                return;
            }
            else
            {
                // Neither was specified, and this page does not have values already set. This is invalid.
                Debug.WriteLine("An instance of {0} received neither a category ID or a selected category when navigated to. Navigation aborted.", new string[] { nameof(CategoryDetailsPageViewModel) });
                await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                            INVALID_STATE_TEXT,
                                                            OK_TEXT);
                _ = await this.NavigationService.GoBackAsync();
                return;
            }
        }
        #endregion Methods
    }
}
