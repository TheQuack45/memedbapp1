﻿using MemeDbApp1.Utility;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MemeDbApp1.ViewModels
{
    public class AltSharePageViewModel : ViewModelBase
    {
        #region Members
        #region Static members
        private const string TEMP_FILE_NAME = "tmsmeme";
        #endregion Static members

        #region Fields
        private IPublicImageSaver _imageSaver;
        private string _imagePath;
        #endregion Fields
        #endregion Members

        #region Constructors
        public AltSharePageViewModel(INavigationService navService, IPublicImageSaver imageSaver)
            : base(navService)
        {
            this._imageSaver = imageSaver;
        }
        #endregion Constructors

        #region Methods
        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            string currentPlatform = Device.RuntimePlatform;
            if (currentPlatform != Device.iOS)
            {
                // Removing the photo from the album is not supported on iOS, but is on Android.
                string extension = MimeManager.ExtensionFromPath(this._imagePath);
                this._imageSaver.RemoveFromPublicDirectory(TEMP_FILE_NAME, extension);
            }

            base.OnNavigatedFrom(parameters);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            string imagePath = parameters.GetValue<string>("ImagePath");
            string mimeType = parameters.GetValue<string>("ImageMimeType");

            this._imagePath = imagePath;
            this._imageSaver.SaveToPublicDirectory(imagePath, TEMP_FILE_NAME, mimeType);

            base.OnNavigatedTo(parameters);
        }
        #endregion Methods
    }
}
