﻿using MemeDbApp1.Models;
using MemeDbApp1.Utility;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MemeDbApp1.ViewModels
{
    public class ImageListPageViewModel : ViewModelBase, INavigationAware
    {
        #region Members
        #region Static members
        public enum SORT_ATTRIBUTE { Invalid, DateSaved, Caption };

        private const string INVALID_STATE_TITLE = "Invalid State";
        private const string INVALID_STATE_TEXT = "This page's state is invalid. Please try whatever you were doing again, and if the issue continues please contact support.";

        private const string OK_TEXT = "OK";
        #endregion Static members

        #region View-bound property/field pairs
        private string _searchText;
        public string SearchText
        {
            get => this._searchText;
            set => this.SetProperty(ref this._searchText, value, nameof(SearchText));
        }

        private ObservableCollection<SortableProperty> _imageSortableProperties;
        public ObservableCollection<SortableProperty> ImageSortableProperties
        {
            get => this._imageSortableProperties;
            set => this.SetProperty(ref this._imageSortableProperties, value, nameof(this.ImageSortableProperties));
        }

        private bool _isFilterPanelVisible;
        public bool IsFilterPanelVisible
        {
            get => this._isFilterPanelVisible;
            set => this.SetProperty(ref this._isFilterPanelVisible, value, nameof(this.IsFilterPanelVisible));
        }

        public string SortOrderToggleIcon
        {
            get
            {
                if (this.IsSortAscending)
                    { return "AscendingIcon"; }
                else
                    { return "DescendingIcon"; }
            }
        }

        private bool _isSortAscending;
        public bool IsSortAscending
        {
            get => this._isSortAscending;
            set
            {
                this.SetProperty(ref this._isSortAscending, value, nameof(this.IsSortAscending));
                this.RaisePropertyChanged(nameof(SortOrderToggleIcon));
            }
        }

        private bool _isSortPanelVisible;
        public bool IsSortPanelVisible
        {
            get => this._isSortPanelVisible;
            set => this.SetProperty(ref this._isSortPanelVisible, value, nameof(this.IsSortPanelVisible));
        }

        private SortableProperty _selectedSortProperty;
        public SortableProperty SelectedImageSortProperty
        {
            get => this._selectedSortProperty;
            set => this.SetProperty(ref this._selectedSortProperty, value, nameof(this.SelectedImageSortProperty));
        }

        private ObservableCollection<Models.Meme> _images;
        public ObservableCollection<Models.Meme> Images
        {
            get { return this._images; }
            set { this.SetProperty(ref this._images, value, nameof(this.Images)); }
        }

        private Models.Meme _selectedImage;
        public Models.Meme SelectedImage
        {
            get { return this._selectedImage; }
            set { this.SetProperty(ref this._selectedImage, value, nameof(this.SelectedImage)); }
        }
        #endregion View-bound property/field pairs

        #region Commands
        private Command _addImageCommand;
        public Command AddImageCommand
        {
            get
            {
                if (this._addImageCommand == null)
                    { this._addImageCommand = new Command(this.AddNewImage); }
                return this._addImageCommand;
            }
        }

        private Command _searchForImagesCommand;
        public Command SearchForImagesCommand
        {
            get
            {
                if (this._searchForImagesCommand == null)
                    { this._searchForImagesCommand = new Command(this.PopulateImages); }
                return this._searchForImagesCommand;
            }
        }

        private Command _selectCategoriesTappedCommand;
        public Command SelectCategoriesTappedCommand
        {
            get
            {
                if (this._selectCategoriesTappedCommand == null)
                    { this._selectCategoriesTappedCommand = new Command(this.FilterImagesByCategory); }
                return this._selectCategoriesTappedCommand;
            }
        }

        private Command _filterPanelTappedCommand;
        public Command FilterPanelTappedCommand
        {
            get
            {
                if (this._filterPanelTappedCommand == null)
                    { this._filterPanelTappedCommand = new Command(this.ToggleFilterPanelVisibility); }
                return this._filterPanelTappedCommand;
            }
        }

        private Command _sortOrderToggleTappedCommand;
        public Command SortOrderToggleTappedCommand
        {
            get
            {
                if (this._sortOrderToggleTappedCommand == null)
                    { this._sortOrderToggleTappedCommand = new Command(this.ToggleSortOrder); }
                return this._sortOrderToggleTappedCommand;
            }
        }

        private Command _sortPanelTappedCommand;
        public Command SortPanelTappedCommand
        {
            get
            {
                if (this._sortPanelTappedCommand == null)
                    { this._sortPanelTappedCommand = new Command(this.ToggleSortPanelVisibility); }
                return this._sortPanelTappedCommand;
            }
        }

        private Command _updateIndexSelectionCommand;
        public Command UpdateIndexSelectionCommand
        {
            get
            {
                if (this._updateIndexSelectionCommand == null)
                    //{ this._updateIndexSelectionCommand = new Command(this.SortImages); }
                    { this._updateIndexSelectionCommand = new Command(this.PopulateImages); }
                return this._updateIndexSelectionCommand;
            }
        }

        private Command _imageTapped;
        public Command ImageTapped
        {
            get
            {
                if (this._imageTapped == null)
                    { this._imageTapped = new Command(this.ViewImageDetails); }
                return this._imageTapped;
            }
        }
        #endregion Commands

        #region Fields
        private List<Models.Meme> _imagesSearched;
        private List<Models.Meme> _imagesFiltered;
        private List<Guid> _selectedFilterCategoryIds;
        private IPageDialogService _dialogService;
        #endregion Fields
        #endregion Members

        #region Constructors
        public ImageListPageViewModel(INavigationService navigationService, IPageDialogService dialogService)
            : base(navigationService)
        {
            Title = "Main Page";

            this.IsSortPanelVisible = false;
            this.IsSortAscending = false;
            this._dialogService = dialogService;
        }
        #endregion Constructors

        #region Methods
        #region Search methods
        private List<Models.Meme> Search(string[] keywords)
        {
            if (keywords == null)
                { throw new ArgumentNullException(nameof(keywords)); }

            List<Models.Meme> selectedImages = new List<Models.Meme>();
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.Meme == null)
                {
                    Debug.WriteLine("dbContext.Image was null when {0}.{1} tried to access it. Cannot continue.",
                                    nameof(ImageListPageViewModel),
                                    Utility.MethodName.Get());
                    throw new InvalidOperationException("dbContext.Image was null.");
                }

                if (keywords.Length == 0)
                {
                    // No keywords were specified. Return all images.
                    selectedImages = dbContext.Meme.ToList();
                }
                else
                {
                    foreach (string keyword in keywords)
                    {
                        // NOTE: This is a very simple search algorithm. Could be replaced by a better one in the future.
                        var currentImages = dbContext.Meme.Where(image => image.Caption.ToLower().Contains(keyword.ToLower()));
                        selectedImages.AddRange(currentImages);
                    }
                }
            }

            return selectedImages;
        }

        public async void UpdateSearchedImages()
        {
            string searchText = this.SearchText;

            if (string.IsNullOrWhiteSpace(searchText))
            {
                // No search text was specified. We should select all images from the database.
                searchText = "";
            }
            string[] keywords = searchText.Split(' ');

            List<Models.Meme> images;
            try
            {
                images = this.Search(keywords);
            }
            catch (InvalidOperationException)
            {
                await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                            INVALID_STATE_TEXT,
                                                            OK_TEXT);
                _ = await this.NavigationService.GoBackAsync();
                return;
            }
            this._imagesSearched = images;
        }
        #endregion Search methods

        #region Filter panel methods
        /// <summary>
        /// Updates the local list of images that have met the filter.
        /// </summary>
        private void UpdateFilteredImages()
        {
            if (this._selectedFilterCategoryIds == null)
            {
                Debug.WriteLine("{0} was null when {1}.{2}() tried to access it. It was assigned a default value of a blank List<Guid>.",
                                nameof(this._selectedFilterCategoryIds),
                                nameof(ImageListPageViewModel), Utility.MethodName.Get());
                this._selectedFilterCategoryIds = new List<Guid>();
            }

            IEnumerable<Guid> categoryIds = this._selectedFilterCategoryIds;

            List<Models.Meme> filteredImages = new List<Models.Meme>();
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (this._imagesSearched == null || this._imagesSearched.Count == 0)
                {
                    Debug.WriteLine("{0} was null when {1}.{2} tried to access it. No filter will be applied.",
                                    nameof(this._imagesSearched),
                                    nameof(ImageListPageViewModel),
                                    Utility.MethodName.Get());
                    // Uses the full list of images if available. If null, uses an empty list.
                    this._imagesFiltered = dbContext.Meme?.ToList() ?? new List<Models.Meme>();
                    return;
                }

                if (categoryIds.Count() == 0)
                {
                    filteredImages = this._imagesSearched;
                }
                else
                {
                    // Select each image that has the selected categories and add it to filteredImages.
                    foreach (Models.Meme image in this._imagesSearched)
                    {
                        // The categories that the image has assigned to it.
                        var imageCategoryIds = dbContext.MemeCategory.Where(ic => ic.MemeId == image.Id)
                                                                        .Select(ic => ic.CategoryId);

                        // The image is valid if it has every one of the required categories.
                        bool isImageValid = categoryIds.All(id => imageCategoryIds.Contains(id));

                        if (isImageValid)
                            { filteredImages.Add(image); }
                    }
                }
            }

            this._imagesFiltered = filteredImages;
        }

        private void FilterImagesByCategory()
        {
            IEnumerable<Guid> selectedCategoryIds = this._selectedFilterCategoryIds ?? new List<Guid>();

            var navParams = new NavigationParameters();
            navParams.Add("CategoryIds", selectedCategoryIds);
            this.NavigationService.NavigateAsync(Utility.NavPath.Get(nameof(CategorySelectionPageViewModel)), navParams);
        }

        private void ToggleFilterPanelVisibility()
        {
            this.IsFilterPanelVisible = !this.IsFilterPanelVisible;
        }
        #endregion Filter panel methods

        #region Sort panel methods
        private void ToggleSortOrder()
        {
            this.IsSortAscending = !this.IsSortAscending;
            this.SortImages();
        }

        private void ToggleSortPanelVisibility()
        {
            this.IsSortPanelVisible = !this.IsSortPanelVisible;
        }

        private void SortImages()
        {
            SortableProperty chosenProperty = this.SelectedImageSortProperty;
            if (chosenProperty == null)
            {
                Debug.WriteLine("{0} was null when {1}.{2} tried to access it. No sort will be applied.",
                                nameof(this.SelectedImageSortProperty),
                                nameof(ImageListPageViewModel),
                                Utility.MethodName.Get());
                this.Images = new ObservableCollection<Models.Meme>(this._imagesFiltered);
                return;
            }

            if (this._imagesFiltered == null)
                { this._imagesFiltered = new List<Models.Meme>(); }

            // Uses the Reflection.MethodInfo ValueGetter to evaluate the property for each image. This way, each possible property is treated exactly the same during sorting.
            IEnumerable<Models.Meme> imagesOrdered = this.IsSortAscending ? this._imagesFiltered.OrderBy(image => chosenProperty.ValueGetter.Invoke(image, null))
                                                                           : this._imagesFiltered.OrderByDescending(image => chosenProperty.ValueGetter.Invoke(image, null));

            this.Images = new ObservableCollection<Models.Meme>(imagesOrdered);
        }
        #endregion Sort panel methods

        private async void AddNewImage()
        {
            MediaFile selectedPicture = await CrossMedia.Current.PickPhotoAsync();
            if (selectedPicture == null)
            {
                // No picture was selected. Do nothing.
                return;
            }

            string mimeType = MimeManager.MimeTypeFromPath(selectedPicture.Path);
            if (string.IsNullOrWhiteSpace(mimeType))
            {
                // No valid MIME type was retrieved. Do nothing.
                return;
            }

            DateTime timeAdded = DateTime.UtcNow;
            var navParams = new NavigationParameters();
            navParams.Add("ImagePath", selectedPicture.Path);
            navParams.Add("ImageMimeType", mimeType);
            navParams.Add("TimeAddedUtc", timeAdded);

            await this.NavigationService.NavigateAsync(nameof(ImageAdditionPageViewModel), navParams);
        }

        private void PopulateImages()
        {
            this.UpdateSearchedImages();
            this.UpdateFilteredImages();
            this.SortImages();
        }

        private async void ViewImageDetails()
        {
            Models.Meme selectedItem = this.SelectedImage;

            this.SelectedImage = null;

            Guid imageId = selectedItem.Id;

            INavigationParameters navParams = new NavigationParameters();
            navParams.Add("Id", imageId);
            await this.NavigationService.NavigateAsync(Utility.NavPath.Get(nameof(ImageDetailsPageViewModel)), navParams);
        }

        public async void RunStartupActions()
        {
            await CrossPermissions.Current.RequestPermissionsAsync(new[] { Plugin.Permissions.Abstractions.Permission.Storage });

            if (this.ImageSortableProperties == null || this.ImageSortableProperties.Count == 0)
            {
                // Initial navigation, nothing is populated yet.
                this._selectedFilterCategoryIds = new List<Guid>();

                Type imageType = typeof(Models.Meme);
                this.ImageSortableProperties = new ObservableCollection<SortableProperty>(SortableProperty.GetSortableProperties(imageType));
                this.SelectedImageSortProperty = this.ImageSortableProperties.Single(sp => sp.Key == "DateAdded");
            }

            this.PopulateImages();
        }

        #region Event handlers
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.TryGetValue("CategoryIds", out List<Guid> categoryIds))
            {
                // This was navigated back to from CategorySelectionPage.
                if (categoryIds == null)
                {
                    // The user pressed cancel. Change nothing.
                    return;
                }

                Task.Run(() =>
                {
                    this._selectedFilterCategoryIds = categoryIds;
                    this.PopulateImages();
                });
            }
            else
            {
                // This was an initial navigation, just from MainPage.
                Device.BeginInvokeOnMainThread(new Action(this.RunStartupActions));
            }
        }
        #endregion Event handlers
        #endregion Methods
    }

    /// <summary>
    /// Represents a property of a model class that can be sorted by.
    /// </summary>
    public class SortableProperty : BindableBase
    {
        private string _key;
        /// <summary>
        /// A unique name for this property. Usually directly matches the identifier for the property in the model class.
        /// </summary>
        public string Key
        {
            get => this._key;
            private set => this.SetProperty(ref this._key, value, nameof(Key));
        }

        private string _displayName;
        /// <summary>
        /// A name for this property that will be displayed to the user when they are selecting it.
        /// </summary>
        public string DisplayName
        {
            get => this._displayName;
            set => this.SetProperty(ref this._displayName, value, nameof(DisplayName));
        }

        /// <summary>
        /// A Reflection.MethodInfo object that, when invoked on an instance of the model class, will return the value of the property in that instance.
        /// </summary>
        public MethodInfo ValueGetter;
        /// <summary>
        /// The data type of the property this is based on.
        /// </summary>
        public Type PropertyType;

        public SortableProperty(string key,
                                string displayName,
                                MethodInfo propertyGetter,
                                Type propertyType)
        {
            this.Key = key;
            this.DisplayName = displayName;
            this.ValueGetter = propertyGetter;
            this.PropertyType = propertyType;
        }

        /// <summary>
        /// Returns a list with a SortableProperty instance for every property on the given type that has SortableAttribute applied to it.
        /// </summary>
        /// <param name="modelType">System.Type that is the type to get sortable properties for.</param>
        /// <returns>List&lt;SortableProperty&gt; where each element represents a sortable property on the given type.</returns>
        public static List<SortableProperty> GetSortableProperties(Type modelType)
        {
            // This is some crazy reflection dark magic.

            if (modelType == null)
            {
                throw new ArgumentNullException(nameof(modelType));
            }

            List<SortableProperty> properties = new List<SortableProperty>();

            // Use reflection to get all properties of the given type that have SortableAttribute applied to them.
            IEnumerable<PropertyInfo> reflProperties = modelType.GetProperties().Where(prop => prop.IsDefined(typeof(SortableAttribute), inherit: false));
            foreach (PropertyInfo reflProp in reflProperties)
            {
                // For each of these properties, create a SortableProperty instance that contains all the information we need to use it for sorting.

                // Get values from the SortableAttribute that was applied to the property.
                SortableAttribute sortableAttribute = reflProp.GetCustomAttribute<SortableAttribute>();
                string name = sortableAttribute.Key;
                string displayName = sortableAttribute.DisplayName;

                // Create a 'delegate' of sorts that can be used to get the value of this property at runtime.
                MethodInfo reflPropGetter = reflProp.GetMethod;
                Type reflPropType = reflProp.PropertyType;

                SortableProperty sortableProperty = new SortableProperty(name,
                                                                         displayName,
                                                                         reflPropGetter,
                                                                         reflPropType);
                properties.Add(sortableProperty);
            }

            return properties;
        }
    }
}
