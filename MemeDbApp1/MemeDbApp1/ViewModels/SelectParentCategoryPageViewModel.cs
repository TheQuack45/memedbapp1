﻿using MemeDbApp1.Models;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MemeDbApp1.ViewModels
{
    public class SelectParentCategoryPageViewModel : ViewModelBase, INavigationAware
    {
        #region Members
        private ObservableCollection<Category> _categories;
        public ObservableCollection<Category> Categories
        {
            get { return this._categories; }
            set { this.SetProperty(ref this._categories, value, "Categories"); }
        }

        private Category _selectedCategory;
        public Category SelectedCategory
        {
            get { return this._selectedCategory; }
            set { this.SetProperty(ref this._selectedCategory, value, "SelectedCategory"); }
        }

        #region Command members
        private Command _selectNoParentCommand;
        public Command SelectNoParentCommand
        {
            get
            {
                if (this._selectNoParentCommand == null)
                    { this._selectNoParentCommand = new Command(this.SelectNoParent); }
                return this._selectNoParentCommand;
            }
        }

        private Command _categorySelectedCommand;
        public Command CategorySelectedCommand
        {
            get
            {
                if (this._categorySelectedCommand == null)
                    { this._categorySelectedCommand = new Command(this.CategorySelected); }
                return this._categorySelectedCommand;
            }
        }

        private Command _cancelCommand;
        public Command CancelCommand
        {
            get
            {
                if (this._cancelCommand == null)
                    { this._cancelCommand = new Command(this.Cancel); }
                return this._cancelCommand;
            }
        }
        #endregion Command members
        #endregion Members

        #region Constructors
        public SelectParentCategoryPageViewModel(INavigationService navService)
            : base(navService)
        {
            this.Title = "Select Parent Category";
        }
        #endregion Constructors

        #region Methods
        private void SelectNoParent()
        {
            this.SelectedCategory = null;
            this.CategorySelected();
        }

        private async void CategorySelected()
        {
            Category selectedCategory = this.SelectedCategory;
            
            INavigationParameters navParams = new NavigationParameters();
            navParams.Add("SelectedCategory", selectedCategory);
            await this.NavigationService.GoBackAsync(navParams);
        }

        private async void Cancel()
        {
            await this.NavigationService.GoBackAsync();
        }

        private void RunStartupActions(Category childCategory)
        {
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                IEnumerable<Category> categories;
                if (childCategory != null)
                {
                    // Child category was specified to exclude.
                    categories = dbContext.Category.Where(category => category.Id != childCategory.Id) // Cannot select category as its own parent.
                                                   .Where(category => !childCategory.Children.Select(c => c.Id).Contains(category.Id)); // Cannot select category's child as a parent.
                }
                else
                {
                    // No child category was specified to exclude.
                    categories = dbContext.Category;
                }
                this.Categories = new ObservableCollection<Category>(categories);
            }
        }

        #region Event handlers
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (!parameters.TryGetValue("ChildCategory", out Category childCategory))
            {
                childCategory = null;
            }

            Task.Run(() => this.RunStartupActions(childCategory));
        }
        #endregion Event handlers
        #endregion Methods
    }
}
