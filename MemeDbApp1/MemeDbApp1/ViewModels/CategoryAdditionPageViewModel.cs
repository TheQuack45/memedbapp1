﻿using MemeDbApp1.Models;
using Microsoft.EntityFrameworkCore;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace MemeDbApp1.ViewModels
{
    public class CategoryAdditionPageViewModel : ViewModelBase, INavigationAware
    {
        #region Members
        #region Static members
        private const string OK_TEXT = "OK";

        private const string INVALID_NAME_TITLE = "Cannot Create";
        private const string INVALID_NAME_PROMPT = "Please specify a name for this category.";

        private const string INVALID_STATE_TITLE = "Invalid State";
        private const string INVALID_STATE_TEXT = "This page's state is invalid. Please try whatever you were doing again, and if the issue continues please contact support.";

        private const string FAILED_SAVE_TITLE = "Failed to Save";
        private const string FAILED_SAVE_TEXT = "Failed to save the new category to the database. Please try again, and if the issue continues please contact support.";
        #endregion Static members

        private string _name;
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty(ref this._name, value, "Name"); }
        }

        public string SelectParentButtonText
        {
            get
            {
                string parentName = (this._parentCategory == null ? "None" : this._parentCategory.Name);
                return string.Format("Parent: {0}", parentName);
            }
        }

        private Category _parentCategory;
        private IPageDialogService _dialogService;

        #region Command members
        private Command _saveCommand;
        public Command SaveCommand
        {
            get
            {
                if (this._saveCommand == null)
                    { this._saveCommand = new Command(this.SaveCategoryAsync); }
                return this._saveCommand;
            }
        }

        private Command _cancelCommand;
        public Command CancelCommand
        {
            get
            {
                if (this._cancelCommand == null)
                    { this._cancelCommand = new Command(this.CancelSave); }
                return this._cancelCommand;
            }
        }

        private Command _selectParentCategoryCommand;
        public Command SelectParentCategoryCommand
        {
            get
            {
                if (this._selectParentCategoryCommand == null)
                    { this._selectParentCategoryCommand = new Command(this.SelectParentCategory); }
                return this._selectParentCategoryCommand;
            }
        }
        #endregion Command members
        #endregion Members

        #region Constructors
        public CategoryAdditionPageViewModel(INavigationService navService, IPageDialogService dialogService)
            : base(navService)
        {
            this.Title = "Add Category";
            this._dialogService = dialogService;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Generates a unique category ID that is not present in the database.
        /// </summary>
        /// <returns>System.Guid that is a new ID.</returns>
        private Guid GenerateUniqueCategoryId()
        {
            Guid categoryId;
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                do
                {
                    categoryId = Guid.NewGuid();
                } while (dbContext.Category.Any(category => category.Id == categoryId));
            }
            return categoryId;
        }

        private async void SelectParentCategory()
        {
            await this.NavigationService.NavigateAsync(Utility.NavPath.Get(nameof(SelectParentCategoryPageViewModel)));
        }

        private async System.Threading.Tasks.Task<bool> AddCategoryToDatabase(Guid id, string name, Category parentCategory)
        {
            if (id == Guid.Empty)
            {
                Debug.WriteLine("{0}.{1}() received an empty value for `id`.",
                                nameof(CategoryAdditionPageViewModel),
                                Utility.MethodName.Get());
                return false;
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                Debug.WriteLine("{0}.{1}() received an invalid category name.",
                                nameof(CategoryAdditionPageViewModel),
                                Utility.MethodName.Get());
                return false;
            }

            Category newCategory = new Category()
            {
                Id = id,
                Name = name,
                Parent = parentCategory,
            };

            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.Category == null)
                {
                    Debug.WriteLine("dbContext.Category was null when {0}.{1}() tried to access it. This is an invalid state.",
                                    nameof(CategoryAdditionPageViewModel),
                                    Utility.MethodName.Get());
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    await this.NavigationService.GoBackAsync();
                    return false;
                }

                var categories = new List<Category>(dbContext.Category);
                dbContext.Category.Add(newCategory);
                try
                {
                    dbContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException e)
                {
                    Debug.WriteLine("DbUpdateConcurrencyException in {0}.{1}(). Message: {2}",
                                    nameof(CategoryAdditionPageViewModel),
                                    Utility.MethodName.Get(),
                                    e.Message);
                    return false;
                }
                catch (DbUpdateException e)
                {
                    Debug.WriteLine("DbUpdateException in {0}.{1}(). Message: {2}",
                                    nameof(CategoryAdditionPageViewModel),
                                    Utility.MethodName.Get(),
                                    e.Message);
                    return false;
                }
            }

            return true;
        }

        private async void SaveCategoryAsync()
        {
            string categoryName = this.Name;
            if (string.IsNullOrWhiteSpace(categoryName))
            {
                await this._dialogService.DisplayAlertAsync(INVALID_NAME_TITLE,
                                                            INVALID_NAME_PROMPT,
                                                            OK_TEXT);
                return;
            }

            Guid categoryId = this.GenerateUniqueCategoryId();

            Category parent = this._parentCategory;

            bool success = await this.AddCategoryToDatabase(categoryId, categoryName, parent);

            if (!success)
            {
                Debug.WriteLine("{0}.{1}() was unsuccessful in saving the new category to the database.",
                                nameof(CategoryAdditionPageViewModel),
                                Utility.MethodName.Get());
                await this._dialogService.DisplayAlertAsync(FAILED_SAVE_TITLE,
                                                            FAILED_SAVE_TEXT,
                                                            OK_TEXT);
                return;
            }

            Debug.WriteLine("{0}.{1}() was successful in saving the new category to the database.",
                            nameof(CategoryAdditionPageViewModel),
                            Utility.MethodName.Get());
            await this.NavigationService.GoBackAsync();
            return;
        }

        private async void CancelSave()
        {
            await this.NavigationService.GoBackAsync();
        }

        public override void OnNavigatedTo(INavigationParameters navParams)
        {
            if (!navParams.TryGetValue("SelectedCategory", out Category parentCategory))
            {
                // No parent category was specified. This was navigated to by EditCategoriesPage, or it was navigated to by SelectParentCategoryPage when the user pressed cancel.
                return;
            }
            else
            {
                // Parent category was specified. This was navigated back to from SelectParentCategoryPage.
                this._parentCategory = parentCategory;
                this.RaisePropertyChanged(nameof(this.SelectParentButtonText));
            }
        }
        #endregion Methods
    }
}
