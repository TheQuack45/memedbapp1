﻿using MemeDbApp1.Models;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MemeDbApp1.ViewModels
{
    public class EditCategoriesPageViewModel : ViewModelBase, INavigationAware
    {
        #region Members
        #region Static members
        private const string OK_TEXT = "OK";

        private const string INVALID_STATE_TITLE = "Invalid State";
        private const string INVALID_STATE_TEXT = "This page's state is invalid. Please try whatever you were doing again, and if the issue continues please contact support.";
        #endregion Static members

        private ObservableCollection<Category> _categories;
        public ObservableCollection<Category> Categories
        {
            get { return this._categories; }
            set { this.SetProperty(ref this._categories, value, "Categories"); }
        }

        private Category _selectedCategory;
        public Category SelectedCategory
        {
            get => this._selectedCategory;
            set => this.SetProperty(ref this._selectedCategory, value, "SelectedCategory");
        }

        private string _searchText;
        public string SearchText
        {
            get => this._searchText;
            set => this.SetProperty(ref this._searchText, value, nameof(SearchText));
        }

        #region Fields
        private IPageDialogService _dialogService;
        #endregion Fields

        #region Command members
        private Command _searchCommand;
        public Command SearchCommand
        {
            get
            {
                if (this._searchCommand == null)
                    { this._searchCommand = new Command(this.UpdateList); }
                return this._searchCommand;
            }
        }

        private Command _addCategoryCommand;
        public Command AddCategoryCommand
        {
            get
            {
                if (this._addCategoryCommand == null)
                    { this._addCategoryCommand = new Command(this.AddCategory); }
                return this._addCategoryCommand;
            }
        }

        private Command _categoryTapped;
        public Command CategoryTapped
        {
            get
            {
                if (this._categoryTapped == null)
                    { this._categoryTapped = new Command(this.CategorySelected); }
                return this._categoryTapped;
            }
        }
        #endregion Command members
        #endregion Members

        #region Constructors
        public EditCategoriesPageViewModel(INavigationService navService, IPageDialogService dialogService)
            : base(navService)
        {
            this.Title = "Categories";
            this._dialogService = dialogService;
        }
        #endregion Constructors

        #region Methods
        private string[] GetSearchKeywords(string searchText)
        {
            if (searchText == null)
                { throw new ArgumentNullException(nameof(searchText)); }

            if (string.IsNullOrWhiteSpace(searchText))
                { return new string[0]; }

            return searchText.Split(' ')
                             .Where(substr => substr != string.Empty) // Remove empty strings. Prevents issues with multiple spaces next to each other.
                             .ToArray();
        }

        private IEnumerable<Category> Search(string[] keywords)
        {
            if (keywords == null)
                { throw new ArgumentNullException(nameof(keywords)); }

            IEnumerable<Category> unfiltered;
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.Category == null)
                    { return null; }

                unfiltered = dbContext.Category.ToList();
            }

            if (keywords.Length == 0)
                { return unfiltered; }

            return unfiltered.Where(category => keywords.Any(keyword => category.Name.ToLower().Contains(keyword.ToLower())));
        }

        private async void UpdateList()
        {
            string[] keywords = this.GetSearchKeywords(this.SearchText);
            IEnumerable<Category> categories = this.Search(keywords);

            if (categories == null)
            {
                await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                            INVALID_STATE_TEXT,
                                                            OK_TEXT);
                _ = await this.NavigationService.GoBackAsync();
                return;
            }

            this.Categories = new ObservableCollection<Category>(categories);
        }

        private async void CategorySelected()
        {
            if (this.SelectedCategory == null)
                { return; }
            Category selectedCategory = this.SelectedCategory;

            var navParams = new NavigationParameters();
            navParams.Add("CategoryId", selectedCategory.Id);
            await this.NavigationService.NavigateAsync(Utility.NavPath.Get(nameof(CategoryDetailsPageViewModel)), navParams);

            this.SelectedCategory = null;
        }

        private async void AddCategory()
        {
            await this.NavigationService.NavigateAsync(Utility.NavPath.Get(nameof(CategoryAdditionPageViewModel)));
        }

        private void RunStartupActions()
        {
            this.SearchText = string.Empty;
            this.UpdateList();
        }

        #region Event handlers
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            Task.Run(new Action(this.RunStartupActions));
        }
        #endregion Event handlers
        #endregion Methods
    }
}
