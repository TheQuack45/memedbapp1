﻿using MemeDbApp1.Models;
using Microsoft.EntityFrameworkCore;
using Plugin.Permissions;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace MemeDbApp1.ViewModels
{
    public class ImageDetailsPageViewModel : ViewModelBase, INavigationAware
    {
        #region Members
        #region Constant members
        // TODO: These should be localizable strings.
        private const string OK_TEXT = "OK";

        private const string DELETE_DIALOG_PROMPT = "Are you sure you want to delete this meme from the database?";
        private const string DELETE_DIALOG_CANCEL = "No, cancel";
        private const string DELETE_DIALOG_YES = "Yes, I'm sure";
        private const string DELETE_DIALOG_YES_FS = "Yes, and delete it from file system";

        private const string DELETE_ERROR_TITLE = "Failed to Delete";
        private const string DELETE_ERROR_TEXT = "Failed to delete your meme. Please try again. If the issue continues, please contact support.";
        private const string DELETE_FS_ERROR_TEXT = "Successfully deleted your meme from the database, but not from the filesystem. If you would like, please delete the picture from your phone manually.";

        private const string DIALOG_ERROR_TITLE = "Failed to Save";
        private const string DIALOG_ERROR_TEXT = "Failed to save your meme. Please try again. If the issue continues, please contact support.";
        private const string DIALOG_ERROR_PROMPT_TEMPLATE = "Failed to save your meme. Please try again. If the issue continues, contact support and send them this error message: {0}";

        private const string INVALID_STATE_TITLE = "Invalid State";
        private const string INVALID_STATE_TEXT = "This page's state is invalid. Please try whatever you were doing again, and if the issue continues please contact support.";

        private const string STORAGE_PERMISSION_TITLE = "Need Storage Permission";
        private const string STORAGE_PERMISSION_TEXT = "The app needs permission to edit storage to be able to delete your meme. Please try again, granting permission this time. If that does not work, please contact support.";

        private const string SHARE_DIALOG_TITLE = "Share Meme";
        #endregion Constant members

        #region Properties
        private ImageSource _image;
        public ImageSource Image
        {
            get { return this._image; }
            set { this.SetProperty(ref this._image, value, nameof(Image)); }
        }

        private string _imageCaption;
        public string ImageCaption
        {
            get { return this._imageCaption; }
            set { this.SetProperty(ref this._imageCaption, value, "ImageCaption"); }
        }

        private Guid _imageId;
        public Guid ImageId
        {
            get { return this._imageId; }
            set { this.SetProperty(ref this._imageId, value, "ImageId"); }
        }

        public DateTime _timeAdded;
        public DateTime TimeAdded
        {
            get => this._timeAdded;
            set => this.SetProperty(ref this._timeAdded, value, nameof(TimeAdded));
        }
        #endregion Properties

        #region Commands
        private Command _altShareImageCommand;
        public Command AltShareImageCommand
        {
            get
            {
                if (this._altShareImageCommand == null)
                    { this._altShareImageCommand = new Command(this.AltShareImage); }
                return this._altShareImageCommand;
            }
        }

        private Command _shareImageCommand;
        public Command ShareImageCommand
        {
            get
            {
                if (this._shareImageCommand == null)
                    { this._shareImageCommand = new Command(this.ShareImage); }
                return this._shareImageCommand;
            }
        }

        private Command _updateCaptionCommand;
        public Command UpdateCaptionCommand
        {
            get
            {
                if (this._updateCaptionCommand == null)
                    { this._updateCaptionCommand = new Command(this.OnUpdateCaption); }
                return this._updateCaptionCommand;
            }
        }

        private Command _deleteImageCommand;
        public Command DeleteImageCommand
        {
            get
            {
                if (this._deleteImageCommand == null)
                    { this._deleteImageCommand = new Command(this.OnDeleteImage); }
                return this._deleteImageCommand;
            }
        }

        private Command _selectCategoriesCommand;
        public Command SelectCategoriesCommand
        {
            get
            {
                if (this._selectCategoriesCommand == null)
                    { this._selectCategoriesCommand = new Command(this.OnSelectCategories); }
                return this._selectCategoriesCommand;
            }
        }
        #endregion Commands

        #region Fields
        private List<Guid> _imageCategoryIds;
        private IPageDialogService _dialogService;
        // _isDeleted is used to keep UpdateCaption() from trying to update the caption in the database when the image has been deleted.
        private bool _isDeleted;
        #endregion Fields
        #endregion Members

        #region Constructors
        public ImageDetailsPageViewModel(INavigationService navService, IPageDialogService dialogService)
            : base(navService)
        {
            this._dialogService = dialogService;
            
            this._isDeleted = false;
        }
        #endregion Constructors

        #region Methods
        private async void AltShareImage()
        {
            if (this.ImageId == Guid.Empty)
            {
                // There is no image ID.
                await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                            INVALID_STATE_TEXT,
                                                            OK_TEXT);
                return;
            }

            string imagePath, mimeType;
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                Models.Meme image = dbContext.Meme.Single(img => img.Id == this.ImageId);
                imagePath = image.Path;
                mimeType = image.MimeType;
            }

            var navParams = new NavigationParameters();
            navParams.Add("ImagePath", imagePath);
            navParams.Add("ImageMimeType", mimeType);

            await this.NavigationService.NavigateAsync(Utility.NavPath.Get(nameof(AltSharePageViewModel)), navParams);
        }

        private async void ShareImage()
        {
            if (this.ImageId == Guid.Empty)
            {
                // There is no image ID.
                await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                            INVALID_STATE_TEXT,
                                                            OK_TEXT);
                return;
            }

            string imagePath, mimeType;
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                Models.Meme image = dbContext.Meme.Single(img => img.Id == this.ImageId);
                imagePath = image.Path;
                mimeType = image.MimeType;
            }

            var file = new ShareFile(imagePath, mimeType);
            await Share.RequestAsync(new ShareFileRequest(SHARE_DIALOG_TITLE, file));
        }

        /// <summary>
        /// Updates the local storage of categories based on the database.
        /// </summary>
        private async void UpdateCategories()
        {
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.MemeCategory == null)
                {
                    Debug.WriteLine("dbContext.ImageCategories was null when {0}.{1}() tried to access it. Category update failed.",
                                    nameof(ImageDetailsPageViewModel),
                                    Utility.MethodName.Get());
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    await this.NavigationService.GoBackAsync();
                    return;
                }

                var categoryIds = dbContext.MemeCategory.Where(ic => ic.MemeId == this.ImageId)
                                                           .Select(ic => ic.CategoryId)
                                                           .ToList();
                this._imageCategoryIds = categoryIds;
            }
        }

        private async Task LoadImageDetails(Guid imageId)
        {
            if (imageId == Guid.Empty)
            {
                // No valid Guid was specified.
                Debug.WriteLine("An invalid GUID was provided to an instance of {0} during navigation.", new string[] { nameof(ImageDetailsPageViewModel) });
                await this.NavigationService.GoBackAsync();
                return;
            }

            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.Meme == null)
                {
                    Debug.WriteLine("dbContext.Image was null when {0}.{1}() tried to access it. Image loading failed.",
                                    nameof(ImageDetailsPageViewModel),
                                    Utility.MethodName.Get());
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    await this.NavigationService.GoBackAsync();
                    return;
                }

                IQueryable<Models.Meme> selectedImageList = dbContext.Meme.Where(image => image.Id == imageId);
                if (selectedImageList.Count() != 1)
                {
                    // There is either 0 or more than 1 image in the database with the ID given.
                    Debug.WriteLine("{0}.{1}() found either 0 or >1 image in the database with the ID {2}. Image loading failed.",
                                    nameof(ImageDetailsPageViewModel), 
                                    Utility.MethodName.Get(),
                                    imageId.ToString());
                    await this.NavigationService.GoBackAsync();
                    return;
                }
                Models.Meme selectedImage = selectedImageList.Single();

                this.ImageId = imageId;
                this.Image = ImageSource.FromFile(selectedImage.Path);
                this.ImageCaption = selectedImage.Caption;
                this.TimeAdded = selectedImage.DateAdded.GetValueOrDefault().ToLocalTime();
            }

            this.UpdateCategories();
        }

        private void UpdateCategoriesInDb(List<Guid> categoryIds)
        {
            if (categoryIds == null)
            {
                Debug.WriteLine("Navigation parameter SelectedCategoryIds was null when {0}.{1}() tried to access it. Update of categories failed.",
                                nameof(ImageDetailsPageViewModel),
                                Utility.MethodName.Get());
                return;
            }

            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                // Select the image that this page is viewing from the database.
                IQueryable<Models.Meme> selectedImageList = dbContext.Meme.Where(image => image.Id == this.ImageId);
                if (selectedImageList.Count() != 1)
                {
                    // There is either 0 or more than 1 image in the database with the ID given.
                    Debug.WriteLine("{0}.{1}() found either 0 or >1 image in the database with the ID {2}. Update of categories failed.",
                                    nameof(ImageDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    this.ImageId.ToString());
                    this.NavigationService.GoBackAsync();
                    return;
                }
                Models.Meme selectedImage = selectedImageList.Single();

                List<Category> originalCategories = dbContext.Category.Where(category => this._imageCategoryIds.Contains(category.Id)).ToList();

                // TODO: This has rapidly approached the point of ugliness. Can I fix it?
                // Will it scale well with high numbers of categories?
                var selectedCategories = dbContext.Category.Where(cat => categoryIds.Contains(cat.Id)).ToList();
                // 1. Remove categories that the user deselected from the database.
                foreach (Category category in originalCategories)
                {
                    if (!selectedCategories.Contains(category))
                    {
                        // The user DESELECTED this category. Remove it.
                        var imageCategory = new MemeCategory { Meme = selectedImage,
                                                                Category = category, };

                        dbContext.MemeCategory.Remove(imageCategory);
                    }
                }
                // 2. Add categories that the user selected to the database.
                foreach (Category category in selectedCategories)
                {
                    var imageCategory = new MemeCategory { Meme = selectedImage,
                                                            MemeId = selectedImage.Id,
                                                            Category = category,
                                                            CategoryId = category.Id, };

                    //if (!dbContext.ImageCategories.Contains(imageCategory))
                    if (!dbContext.MemeCategory.Any(ic => (ic.Meme == imageCategory.Meme && ic.Category == imageCategory.Category) ||
                                                             (ic.MemeId == imageCategory.MemeId && ic.CategoryId == imageCategory.CategoryId)))
                    {
                        // This category is NOT already paired with the image in the database. Add it.
                        dbContext.MemeCategory.Add(imageCategory);
                    }
                }

                try
                {
                    dbContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException e)
                {
                    Debug.WriteLine("DbUpdateConcurrencyException in {0}.{1}(). Message: {2}",
                                    nameof(ImageDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    e.Message);
                }
                catch (DbUpdateException e)
                {
                    Debug.WriteLine("DbUpdateException in {0}.{1}(). Message: {2}",
                                    nameof(ImageDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    e.Message);
                }
            }

            this.UpdateCategories();
        }

        #region Command handlers
        private void OnSelectCategories()
        {
            IEnumerable<Guid> categoryIds = this._imageCategoryIds;

            var navParams = new NavigationParameters();
            navParams.Add("CategoryIds", categoryIds);
            this.NavigationService.NavigateAsync(Utility.NavPath.Get(nameof(CategorySelectionPageViewModel)), navParams);
        }

        private async void OnDeleteImage()
        {
            // Confirm that the user wants to delete the image.
            bool deleteFromFileSystem = false;
            string response = await this._dialogService.DisplayActionSheetAsync(DELETE_DIALOG_PROMPT,
                                                                                DELETE_DIALOG_CANCEL,
                                                                                null, // There are 2 destructive options, so I can't choose one.
                                                                                DELETE_DIALOG_YES,
                                                                                DELETE_DIALOG_YES_FS);
            // If the user taps outside of the dialog, 'response' will be null. This should be considered a choice of "cancel".
            if (response == null || response == DELETE_DIALOG_CANCEL)
                { return; }
            else if (response == DELETE_DIALOG_YES_FS)
                { deleteFromFileSystem = true; }

            // Retrieve image path and delete image from database.
            Guid id = this.ImageId;

            string imagePath;
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.Meme == null)
                {
                    Debug.WriteLine("dbContext.Meme was null when {0}.{1}() tried to access it. Image deletion failed.",
                                    nameof(ImageDetailsPageViewModel),
                                    Utility.MethodName.Get());
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    _ = await this.NavigationService.GoBackAsync();
                    return;
                }

                IQueryable<Models.Meme> selectedImageList = dbContext.Meme.Where(image => image.Id == id);
                if (selectedImageList.Count() != 1)
                {
                    // There is either 0 or more than 1 image in the database with the ID given.
                    Debug.WriteLine("{0}.{1}() found either 0 or >1 image in the database with the ID {2}. Image deletion failed.",
                                    nameof(ImageDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    id.ToString());
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    _ = await this.NavigationService.GoBackAsync();
                    return;
                }
                Models.Meme selectedImage = selectedImageList.Single();

                imagePath = selectedImage.Path;

                dbContext.Meme.Remove(selectedImage);

                try
                {
                    dbContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException e)
                {
                    string error = $"{nameof(DbUpdateConcurrencyException)} in {nameof(ImageDetailsPageViewModel)}.{Utility.MethodName.Get()}.";
                    Debug.WriteLine(error + " Message: {1}", e.Message);
                    await this._dialogService.DisplayAlertAsync(DELETE_ERROR_TITLE,
                                                                DELETE_ERROR_TEXT,
                                                                OK_TEXT);
                    _ = await this.NavigationService.GoBackAsync();
                    return;
                }
                catch (DbUpdateException e)
                {
                    string error = $"General {nameof(DbUpdateException)} in {nameof(ImageDetailsPageViewModel)}.{Utility.MethodName.Get()}.";
                    Debug.WriteLine(error + " Message: {1}", e.Message);
                    await this._dialogService.DisplayAlertAsync(DELETE_ERROR_TITLE,
                                                                DELETE_ERROR_TEXT,
                                                                OK_TEXT);
                    _ = await this.NavigationService.GoBackAsync();
                    return;
                }

                Debug.WriteLine("{0}.{1}() successfully deleted the image with ID {1} from the database.",
                                nameof(ImageDetailsPageViewModel),
                                Utility.MethodName.Get(),
                                id.ToString());
            }

            this._isDeleted = true;

            if (!deleteFromFileSystem)
            {
                Debug.WriteLine("User did not choose to delete the image with ID {0} from the file system, so {1}.{2} completed without doing so.",
                                id.ToString(),
                                nameof(ImageDetailsPageViewModel),
                                Utility.MethodName.Get());
                await this.NavigationService.GoBackAsync();
                return;
            }

            // Delete image from filesystem.
            var storagePermission = Plugin.Permissions.Abstractions.Permission.Storage;
            var permissionsResults = await CrossPermissions.Current.RequestPermissionsAsync(new[] { storagePermission });
            bool storageGranted = permissionsResults[storagePermission] == Plugin.Permissions.Abstractions.PermissionStatus.Granted;

            if (!storageGranted)
            {
                // Permission for external storage was not granted.
                Debug.WriteLine("{0}.{1}() did not receive permission to edit external storage. Deletion of image with ID {2} from file system failed.",
                                nameof(ImageDetailsPageViewModel),
                                Utility.MethodName.Get(),
                                id.ToString());
                await this._dialogService.DisplayAlertAsync(STORAGE_PERMISSION_TITLE,
                                                            STORAGE_PERMISSION_TEXT,
                                                            OK_TEXT);
                return;
            }

            if (string.IsNullOrWhiteSpace(imagePath))
            {
                Debug.WriteLine("{0}.{1}() deleted from database successfully, but found invalid filesystem path. Continuing.",
                                nameof(ImageDetailsPageViewModel),
                                Utility.MethodName.Get());
                _ = await this.NavigationService.GoBackAsync();
                return;
            }
            else if (!System.IO.File.Exists(imagePath))
            {
                Debug.WriteLine("{0}.{1}() deleted from database successfully, but found no image at the filesystem path. Continuing.",
                                nameof(ImageDetailsPageViewModel),
                                Utility.MethodName.Get());
                _ = await this.NavigationService.GoBackAsync();
                return;
            }

            try
            {
                System.IO.File.Delete(imagePath);
            }
            catch (System.IO.DirectoryNotFoundException e)
            {
                Debug.WriteLine("System.IO.DirectoryNotFoundException in {0}.{1}(). Deletion of image with ID {2} from file system failed. Message: {3}",
                                nameof(ImageDetailsPageViewModel),
                                Utility.MethodName.Get(),
                                id.ToString(),
                                e.Message);
                await this._dialogService.DisplayAlertAsync(DELETE_ERROR_TITLE,
                                                            DELETE_FS_ERROR_TEXT,
                                                            OK_TEXT);
                _ = await this.NavigationService.GoBackAsync();
                return;
            }
            catch (System.IO.PathTooLongException)
            {
                Debug.WriteLine("System.IO.PathTooLongException in {0}.{1}(). Deletion of image with ID {2} from file system failed.",
                                nameof(ImageDetailsPageViewModel),
                                Utility.MethodName.Get(),
                                id.ToString());
                await this._dialogService.DisplayAlertAsync(DELETE_ERROR_TITLE,
                                                            DELETE_FS_ERROR_TEXT,
                                                            OK_TEXT);
                _ = await this.NavigationService.GoBackAsync();
                return;
            }
            catch (System.IO.IOException e)
            {
                Debug.WriteLine("System.IO.IOException in {0}.{1}(). Deletion of image with ID {2} from file system failed. Message: {3}",
                                nameof(ImageDetailsPageViewModel),
                                Utility.MethodName.Get(),
                                id.ToString(),
                                e.Message);
                await this._dialogService.DisplayAlertAsync(DELETE_ERROR_TITLE,
                                                            DELETE_FS_ERROR_TEXT,
                                                            OK_TEXT);
                _ = await this.NavigationService.GoBackAsync();
                return;
            }
            catch (UnauthorizedAccessException e)
            {
                Debug.WriteLine("UnauthorizedAccessException in {0}.{1}(). Deletion of image with ID {2} from file system failed. Message: {3}",
                                nameof(ImageDetailsPageViewModel),
                                Utility.MethodName.Get(),
                                id.ToString(),
                                e.Message);
                await this._dialogService.DisplayAlertAsync(DELETE_ERROR_TITLE,
                                                            DELETE_FS_ERROR_TEXT,
                                                            OK_TEXT);
                _ = await this.NavigationService.GoBackAsync();
                return;
            }

            Debug.WriteLine("{0}.{1}() successfully deleted the image with ID {2} from the file system.",
                            nameof(ImageDetailsPageViewModel),
                            Utility.MethodName.Get(),
                            id.ToString());
            await this.NavigationService.GoBackAsync();
        }

        private void OnUpdateCaption()
        {
            if (this._isDeleted)
                { return; }

            string caption = this.ImageCaption;
            if (string.IsNullOrWhiteSpace(caption))
            {
                caption = null;
            }

            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.Meme == null)
                {
                    Debug.WriteLine("dbContext.Memes was null when {0}.{1}() tried to access it. Caption save failed.",
                                    nameof(ImageDetailsPageViewModel),
                                    Utility.MethodName.Get());
                    this.NavigationService.GoBackAsync();
                    return;
                }

                // I use this method instead of dbContext.Meme.Single(predicate) because the latter method gets complex and ugly; a little moreso than this.
                IQueryable<Models.Meme> selectedImageList = dbContext.Meme.Where(image => image.Id == this.ImageId);
                if (selectedImageList.Count() != 1)
                {
                    // There is either 0 or more than 1 image in the database with the ID given.
                    Debug.WriteLine("{0}.{1}() found either 0 or >1 image in the database with the ID {2}. Caption save failed.",
                                    nameof(ImageDetailsPageViewModel),
                                    Utility.MethodName.Get(),
                                    this.ImageId.ToString());
                    this.NavigationService.GoBackAsync();
                    return;
                }
                Models.Meme selectedImage = selectedImageList.Single();
                // TODO: Is this the best way to update it?
                dbContext.Meme.Update(selectedImage).Entity.Caption = caption;

                dbContext.SaveChanges();
            }
        }
        #endregion Command handlers

        #region Event handlers
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.TryGetValue("CategoryIds", out List<Guid> categoryIds))
            {
                // Navigated back to from CategorySelectionPage.
                if (categoryIds == null)
                {
                    // The user pressed cancel. Change nothing.
                    return;
                }

                this.UpdateCategoriesInDb(categoryIds);
            }
            else if (parameters.TryGetValue("Id", out Guid imageId))
            {
                // Initial navigation, from ImageListPage.
                // Uses a discard variable to suppress CS4014, the warning about an async call not being awaited.
                _  = this.LoadImageDetails(imageId);
            }
            else if (this.ImageId != Guid.Empty)
            {
                // This was navigated back to from AltSharePage.
                return;
            }
            else
            {
                // Invalid scenario. Go back.
                Debug.WriteLine("An instance of {0} received no parameters during navigation. This is an invalid scenario. Going back.",
                                new string[] { nameof(ImageDetailsPageViewModel) });
                this.NavigationService.GoBackAsync();
            }
        }
        #endregion Event handlers
        #endregion Methods
    }
}
