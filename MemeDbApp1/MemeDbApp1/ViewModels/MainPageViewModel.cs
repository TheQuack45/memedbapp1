﻿using MemeDbApp1.Models;
using Prism.Navigation;
using System.Diagnostics;
using Xamarin.Forms;

namespace MemeDbApp1.ViewModels
{
    public class MainPageViewModel : ViewModelBase
	{
        #region Members
        #region Command members
        private Command _navigateCommand;
        public Command NavigateCommand
        {
            get
            {
                if (this._navigateCommand == null)
                    { this._navigateCommand = new Command(this.Navigate); }
                return this._navigateCommand;
            }
        }
        #endregion Command members

        private MainPageMenuItem _selectedMenuItem;
        public MainPageMenuItem SelectedMenuItem
        {
            get { return this._selectedMenuItem; }
            set { this.SetProperty(ref this._selectedMenuItem, value, "SelectedMenuItem"); }
        }
        #endregion Members

        #region Constructors
        public MainPageViewModel(INavigationService navService)
            : base(navService)
        {

        }
        #endregion Constructors

        #region Methods
        public async void Navigate()
        {
            MainPageMenuItem currentItem = this.SelectedMenuItem;
            if (currentItem == null)
            {
                Debug.WriteLine("{0} was null when {1}.Navigate was fired. Navigation failed.", nameof(this.SelectedMenuItem), nameof(MainPageViewModel));
                return;
            }
            string targetViewModel = currentItem.TargetType.Name + "ViewModel";
            await this.NavigationService.NavigateAsync(Utility.NavPath.Get(nameof(NavigationPage), targetViewModel));
            this.SelectedMenuItem = null;
        }
        #endregion Methods
    }
}
