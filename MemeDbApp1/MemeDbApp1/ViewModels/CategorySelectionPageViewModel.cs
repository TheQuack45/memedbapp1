﻿using MemeDbApp1.Models;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MemeDbApp1.ViewModels
{
    public class CategorySelectionPageViewModel : ViewModelBase
    {
        #region Members
        #region Static members
        private const string OK_TEXT = "OK";

        private const string INVALID_STATE_TITLE = "Invalid State";
        private const string INVALID_STATE_TEXT = "This page's state is invalid. Please try whatever you were doing again, and if the issue continues please contact support.";
        #endregion Static members

        private ObservableCollection<SelectableCategory> _categories;
        public ObservableCollection<SelectableCategory> Categories
        {
            get => this._categories;
            set => this.SetProperty(ref this._categories, value, "Categories");
        }

        private SelectableCategory _selectedCategory;
        public SelectableCategory SelectedCategory
        {
            get => this._selectedCategory;
            set => this.SetProperty(ref this._selectedCategory, value, "SelectedCategory");
        }

        private string _searchText;
        public string SearchText
        {
            get => this._searchText;
            set => this.SetProperty(ref this._searchText, value, nameof(SearchText));
        }

        #region Fields
        private List<SelectableCategory> _baseCategories;
        private IPageDialogService _dialogService;
        #endregion Fields

        #region Command members
        private Command _searchCommand;
        public Command SearchCommand
        {
            get
            {
                if (this._searchCommand == null)
                    { this._searchCommand = new Command(this.UpdateList); }
                return this._searchCommand;
            }
        }

        private Command _categorySelectedCommand;
        public Command CategorySelectedCommand
        {
            get
            {
                if (this._categorySelectedCommand == null)
                    { this._categorySelectedCommand = new Command(this.OnCategorySelected); }
                return this._categorySelectedCommand;
            }
        }

        private Command _finishCommand;
        public Command FinishCommand
        {
            get
            {
                if (this._finishCommand == null)
                    { this._finishCommand = new Command(this.OnFinish); }
                return this._finishCommand;
            }
        }

        private Command _cancelCommand;
        public Command CancelCommand
        {
            get
            {
                if (this._cancelCommand == null)
                    { this._cancelCommand = new Command(this.OnCancel); }
                return this._cancelCommand;
            }
        }
        #endregion Command members
        #endregion Members

        #region Constructors
        public CategorySelectionPageViewModel(INavigationService navService, IPageDialogService dialogService)
            : base(navService)
        {
            this.Title = "Select Categories";
            this._dialogService = dialogService;
        }
        #endregion Constructors

        #region Methods
        private string[] GetSearchKeywords(string searchText)
        {
            if (searchText == null)
                { throw new ArgumentNullException(nameof(searchText)); }

            if (string.IsNullOrWhiteSpace(searchText))
                { return new string[0]; }

            return searchText.Split(' ')
                             .Where(substr => substr != string.Empty) // Remove empty strings. Prevents issues with multiple spaces next to each other.
                             .ToArray();
        }

        private IEnumerable<SelectableCategory> Search(string[] keywords)
        {
            if (keywords == null)
                { throw new ArgumentNullException(nameof(keywords)); }

            List<SelectableCategory> unfiltered = this._baseCategories;

            if (keywords.Length == 0)
                { return unfiltered; }

            return unfiltered.Where(category => keywords.Any(keyword => category.Name.ToLower().Contains(keyword.ToLower())));
        }

        /// <summary>
        /// Toggles the selection of the given category.
        /// If it has been deselected, also deselects its children.
        /// If it has been selected, also selects its parents.
        /// </summary>
        /// <param name="category">SelectableCategory that is the category to manipulate.</param>
        private void ToggleSelection(SelectableCategory category)
        {
            if (category == null)
            {
                Debug.WriteLine("{0}.{1}() received a category that was null. Selection toggling failed.",
                                nameof(CategorySelectionPageViewModel), Utility.MethodName.Get());
                return;
            }

            SelectableCategory baseCategory = this._baseCategories.Single(bc => bc.Id == category.Id);
            bool wasSelected = baseCategory.IsSelected;
            baseCategory.IsSelected = !wasSelected;
            if (this.Categories.Any(c => c.Id == category.Id))
            {
                // If this category is currently being displayed (if it's appearing in the search), toggle it.
                // This shouldn't ever happen but we handle it just in case.
                SelectableCategory displayCategory = this.Categories.Single(c => c.Id == category.Id);
                displayCategory.IsSelected = !wasSelected;
            }

            if (wasSelected)
            {
                // It was selected, and now is no longer selected. Deselect its children.
                this.DeselectChildCategories(category);
            }
            // I use an else if here (rather than just an else) because it makes it a little more clear. It's not actually necessary.
            else if (!wasSelected)
            {
                // It was not selected, and is now being selected. Select its parents.
                this.SelectParentCategories(category);
            }
        }

        /// <summary>
        /// If the given SelectableCategory is not selected, deselects all of its child categories by reference.
        /// </summary>
        /// <param name="parent">SelectableCategory that is the category to manipulate the children of.</param>
        private void DeselectChildCategories(SelectableCategory parent)
        {
            if (parent == null)
            {
                Debug.WriteLine("{0}.{1}() received a `parent` category that was null. No child categories were affected.",
                                nameof(CategorySelectionPageViewModel), Utility.MethodName.Get());
                return;
            }

            if (parent.IsSelected)
            {
                // A parent can be selected without its children being selected.
                // Example: A meme can be a Star Wars meme, but not a prequel meme.
                return;
            }

            // A parent cannot be deselected without its children also being deselected.
            // Example: A meme that is not a Star Wars meme (the parent category) cannot be a prequel meme (a child category).
            foreach (Category child in parent.Children)
            {
                Guid childId = child.Id;
                SelectableCategory selectableChild = this._baseCategories.Single(sc => sc.Id == childId);
                selectableChild.IsSelected = false;
                if (this.Categories.Any(c => c.Id == childId))
                {
                    // If this child is currently being displayed (if it's appearing in the search), deselect it.
                    SelectableCategory displayChild = this.Categories.Single(c => c.Id == childId);
                    displayChild.IsSelected = false;
                }

                // Recursively deselect this child's children.
                this.DeselectChildCategories(selectableChild);
            }
        }

        /// <summary>
        /// If the given SelectableCategory is selected, selects all of its parents by reference.
        /// </summary>
        /// <param name="child">SelectableCategory that is the category to manipulate the parents of.</param>
        private void SelectParentCategories(SelectableCategory child)
        {
            if (child == null)
            {
                Debug.WriteLine("{0}.{1}() received a `child` category that was null. No parent categories were selected.",
                                nameof(CategorySelectionPageViewModel), Utility.MethodName.Get());
                return;
            }

            if (!child.IsSelected)
            {
                // The child is not being selected, so we don't need to change the parent at all.
                // Example: Just because a meme is a Star Wars meme, doesn't mean it has to also be a prequel meme (which is a child of Star Wars meme).
                return;
            }

            if (child.Parent == null)
                { return; }

            Guid parentId = child.Parent.Id;
            SelectableCategory selectableParent = this._baseCategories.Single(sc => sc.Id == parentId);
            selectableParent.IsSelected = true;
            if (this.Categories.Any(c => c.Id == parentId))
            {
                // If this parent is currently being displayed (if it's appearing in the search), select it.
                SelectableCategory displayParent = this.Categories.Single(c => c.Id == parentId);
                displayParent.IsSelected = true;
            }

            // Recursively selects the parent's parent, all the way up the chain.
            this.SelectParentCategories(selectableParent);
        }

        private async void RunStartupActions(IEnumerable<Guid> categoryIds)
        {
            using (var dbContext = new InternalDbContext(Utility.FsPaths.MainDatabasePath))
            {
                if (dbContext.Category == null)
                {
                    Debug.WriteLine("InternalDbContext.{0} was null when {1}.{2}() tried to access it. Image category selection failed.",
                                    nameof(dbContext.Category),
                                    nameof(CategorySelectionPageViewModel),
                                    Utility.MethodName.Get());
                    await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                                INVALID_STATE_TEXT,
                                                                OK_TEXT);
                    await this.NavigationService.GoBackAsync();
                    return;
                }
                List<Category> categories = dbContext.Category.ToList();

                this._baseCategories = new List<SelectableCategory>();
                foreach (Category category in categories)
                {
                    if (category == null)
                        { continue; }

                    // TODO: How to store the children without having to go back through and repopulate every category's children?
                    var selectableCategory = new SelectableCategory()
                    {
                        Id = category.Id,
                        Name = category.Name,
                        Parent = category.Parent,
                        Children = category.Children,
                        IsSelected = (categoryIds.Contains(category.Id)),
                    };
                    this._baseCategories.Add(selectableCategory);
                }
            }

            this.Categories = new ObservableCollection<SelectableCategory>(this._baseCategories);
        }

        #region Command handlers
        private void UpdateList()
        {
            string[] keywords = this.GetSearchKeywords(this.SearchText);
            IEnumerable<SelectableCategory> categories = this.Search(keywords);
            this.Categories = new ObservableCollection<SelectableCategory>(categories);
        }

        private void OnFinish()
        {
            List<Guid> selectedIds = this._baseCategories.Where(cat => cat.IsSelected)
                                                         .Select(cat => cat.Id)
                                                         .ToList();

            var navParams = new NavigationParameters();
            navParams.Add("CategoryIds", selectedIds);
            this.NavigationService.GoBackAsync(navParams);
        }

        private async void OnCancel()
        {
            // Pass a null list so the calling page would be able to distinguish between the user pressing cancel, and it being navigated to with no parameters (an invalid scenario).
            var navParams = new NavigationParameters();
            navParams.Add("CategoryIds", null);
            await this.NavigationService.GoBackAsync(navParams);
        }

        private void OnCategorySelected()
        {
            this.ToggleSelection(this.SelectedCategory);
            this.SelectedCategory = null;
        }
        #endregion Command handlers

        #region Event handlers
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            if (!parameters.TryGetValue("CategoryIds", out IEnumerable<Guid> categoryIds))
            {
                Debug.WriteLine("An instance of {0} received no CategoryIds navigation parameter. Image category selection failed.", new string[] { nameof(CategorySelectionPageViewModel) });
                await this._dialogService.DisplayAlertAsync(INVALID_STATE_TITLE,
                                                            INVALID_STATE_TEXT,
                                                            OK_TEXT);
                _ = await this.NavigationService.GoBackAsync();
                return;
            }

            _ = Task.Run(() => this.RunStartupActions(categoryIds));
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            parameters.Add("CategoryIds", null);

            base.OnNavigatedFrom(parameters);
        }
        #endregion Event handlers
        #endregion Methods
    }

    // TODO: Should this move into the Model?
    /// <summary>
    /// Represents a MemeDbApp1.Models.Category that tracks whether it has been selected or not.
    /// </summary>
    public class SelectableCategory : BindableBase
    {
        private Guid _id;
        public Guid Id
        {
            get => this._id;
            set => this.SetProperty(ref this._id, value, "Id");
        }

        private string _name;
        public string Name
        {
            get => this._name;
            set => this.SetProperty(ref this._name, value, "Name");
        }

        private Category _parent;
        public Category Parent
        {
            get => this._parent;
            set => this.SetProperty(ref this._parent, value, "Parent");
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get => this._isSelected;
            set => this.SetProperty(ref this._isSelected, value, "IsSelected");
        }

        private ICollection<Category> _children;
        public ICollection<Category> Children
        {
            get => this._children;
            set => this.SetProperty(ref this._children, value, nameof(this.Children));
        }
    }
}
