﻿using Prism;
using Prism.Ioc;
using MemeDbApp1.ViewModels;
using MemeDbApp1.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System;
using Prism.Navigation;
using System.Linq;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MemeDbApp1
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync(Utility.NavPath.Get(true, nameof(MainPageViewModel), nameof(NavigationPage), nameof(ImageListPageViewModel)));
        }

        protected override void OnStart()
        {
            System.IO.Directory.CreateDirectory(Utility.FsPaths.ImagesDirectory);

            Xamarin.Essentials.ExperimentalFeatures.Enable(Xamarin.Essentials.ExperimentalFeatures.ShareFileRequest);
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>(nameof(MainPageViewModel));
            containerRegistry.RegisterForNavigation<ImageListPage, ImageListPageViewModel>(nameof(ImageListPageViewModel));
            containerRegistry.RegisterForNavigation<ImageAdditionPage, ImageAdditionPageViewModel>(nameof(ImageAdditionPageViewModel));
            containerRegistry.RegisterForNavigation<ImageDetailsPage, ImageDetailsPageViewModel>(nameof(ImageDetailsPageViewModel));
            containerRegistry.RegisterForNavigation<EditCategoriesPage, EditCategoriesPageViewModel>(nameof(EditCategoriesPageViewModel));
            containerRegistry.RegisterForNavigation<CategoryAdditionPage, CategoryAdditionPageViewModel>(nameof(CategoryAdditionPageViewModel));
            containerRegistry.RegisterForNavigation<SelectParentCategoryPage, SelectParentCategoryPageViewModel>(nameof(SelectParentCategoryPageViewModel));
            containerRegistry.RegisterForNavigation<CategorySelectionPage, CategorySelectionPageViewModel>(nameof(CategorySelectionPageViewModel));
            containerRegistry.RegisterForNavigation<CategoryDetailsPage, CategoryDetailsPageViewModel>(nameof(CategoryDetailsPageViewModel));
            containerRegistry.RegisterForNavigation<AltSharePage, AltSharePageViewModel>(nameof(AltSharePageViewModel));
        }

        public async void HandleImageAsync(System.IO.MemoryStream imageStream, string mimeType, DateTime timeAddedUniversal)
        {
            string extension = Utility.MimeManager.MimeTypeToExtension(mimeType);
            string tempFileName = string.Format(@"{0}.{1}",
                                                "temp",
                                                extension);
            string tempSavePath = System.IO.Path.Combine(Utility.FsPaths.MainInternalDirectory, tempFileName);
            System.IO.File.WriteAllBytes(tempSavePath, imageStream.ToArray());
            INavigationParameters navParams = new NavigationParameters();
            navParams.Add("ImagePath", tempSavePath);
            navParams.Add("ImageMimeType", mimeType);
            navParams.Add("TimeAddedUtc", timeAddedUniversal);

            await this.NavigationService.NavigateAsync(Utility.NavPath.Get(true, nameof(MainPageViewModel),
                                                                                 nameof(NavigationPage),
                                                                                 nameof(ImageListPageViewModel),
                                                                                 nameof(ImageAdditionPageViewModel)), navParams);
        }
    }
}
