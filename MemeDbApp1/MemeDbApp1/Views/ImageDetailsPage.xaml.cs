﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MemeDbApp1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ImageDetailsPage : ContentPage
	{
		public ImageDetailsPage ()
		{
			InitializeComponent ();

            if (Device.RuntimePlatform == Device.Android)
            {
                // Alternative share only really works well on Android.
                var altShareItem = new ToolbarItem()
                {
                    Order = ToolbarItemOrder.Secondary,
                    Text = "Alternative Share",
                };
                altShareItem.SetBinding(ToolbarItem.CommandProperty, new Binding("AltShareImageCommand"));
                altShareItem.SetValue(AutomationProperties.IsInAccessibleTreeProperty, true);
                altShareItem.SetValue(AutomationProperties.NameProperty, "Alternative Share Meme");
                altShareItem.SetValue(AutomationProperties.HelpTextProperty, "Press this to share this meme through an alternative method that supports apps like Snapchat");
                Page.ToolbarItems.Add(altShareItem);
            }
        }
	}
}