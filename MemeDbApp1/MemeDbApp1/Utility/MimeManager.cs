﻿using System.Collections.Generic;

namespace MemeDbApp1.Utility
{
    public static class MimeManager
    {
        private static readonly Dictionary<string, string> _mimeTypeToExtensionDict = new Dictionary<string, string>()
        {
            { @"image/jpeg", @"jpg" },
            { @"image/png", @"png" },
        };

        private static readonly Dictionary<string, string> _extensionToMimeTypeDict = new Dictionary<string, string>()
        {
            { @"jpg", @"image/jpeg" },
            { @"jpeg", @"image/jpeg" },
            { @"png", @"image/png" },
        };

        /// <summary>
        /// Returns the MIME type for the file at with the given path. DOES NOT check file content, only based on path.
        /// Returns string.Empty if the given path is null, is empty, is whitespace, does not contain an extension, or does not have a corresponding MIME type.
        /// </summary>
        /// <param name="path">System.String that is a file path.</param>
        /// <returns>System.String that is the MIME type of the file.</returns>
        public static string MimeTypeFromPath(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                // Given path is not valid.
                return string.Empty;
            }

            string extension = ExtensionFromPath(path);
            if (string.IsNullOrWhiteSpace(extension))
            {
                // No extension was retrieved.
                return string.Empty;
            }

            return ExtensionToMimeType(extension);
        }

        /// <summary>
        /// Returns the file extension for the given path. The period prefix is not included.
        /// Returns string.Empty if path is null or does not contain a valid file extension.
        /// </summary>
        /// <param name="path">System.String that is a file path.</param>
        /// <returns>System.String that is the extension of the file.</returns>
        public static string ExtensionFromPath(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                return string.Empty;
            }

            string extensionWithPeriod = System.IO.Path.GetExtension(path);
            if (extensionWithPeriod == string.Empty)
            {
                // If the path did not contain an extension, return string.Empty.
                return string.Empty;
            }
            // Removes period from start of extension.
            string extension = extensionWithPeriod.Substring(1, extensionWithPeriod.Length - 1);

            return extension;
        }

        /// <summary>
        /// Returns a possible file extension for the given MIME type. The period prefix is not included.
        /// Returns string.Empty if given value is not a valid MIME type, or if an extension for it was not found.
        /// </summary>
        /// <param name="mimeType">System.String that is a MIME type.</param>
        /// <returns>System.String that is a possible extension.</returns>
        public static string MimeTypeToExtension(string mimeType)
        {
            string extension;
            if (!_mimeTypeToExtensionDict.TryGetValue(mimeType, out extension))
            {
                return string.Empty;
            }
            return extension;
        }

        /// <summary>
        /// Returns the MIME type for the given file extension.
        /// Returns string.Empty if a MIME type was not found.
        /// </summary>
        /// <param name="extension">System.String that is a file extension. SHALL NOT INCLUDE the period prefix.</param>
        /// <returns>System.String that is a MIME type.</returns>
        public static string ExtensionToMimeType(string extension)
        {
            string mimeType;
            if (!_extensionToMimeTypeDict.TryGetValue(extension, out mimeType))
            {
                return string.Empty;
            }
            return mimeType;
        }
    }
}
