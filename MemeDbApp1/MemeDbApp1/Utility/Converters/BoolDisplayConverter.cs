﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MemeDbApp1.Utility.Converters
{
    public class BoolDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isSelected = (bool)value;
            return isSelected ? @"✓" : @"✗";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
