﻿using MemeDbApp1.Models;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace MemeDbApp1.Utility.Converters
{
    public class CategoryParentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var parent = value as Category;
            return string.Format("Parent: {0}", parent.Name);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
