﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MemeDbApp1.Utility.Converters
{
    public class DateTimeDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                { throw new ArgumentNullException(nameof(value)); }
            var dt = (DateTime)value;

            var formatString = parameter as string;
            if (formatString == null)
                { formatString = ""; }

            return $"Added on {dt.ToString(formatString)}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
