﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MemeDbApp1.Utility.Converters
{
    public class GuidToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var guid = value as Guid?;
            if (guid == Guid.Empty)
                { return ""; }
            else
                { return guid.ToString(); }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var guidString = value as string;
            if (Guid.TryParse(guidString, out Guid guid))
                { return new Guid?(guid); }
            else
                { return null; }
        }
    }
}
