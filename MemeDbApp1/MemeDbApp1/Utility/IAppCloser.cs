﻿namespace MemeDbApp1.Utility
{
    public interface IAppCloser
    {
        void CloseApplication();
    }
}
