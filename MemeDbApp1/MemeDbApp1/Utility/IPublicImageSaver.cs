﻿using System;

namespace MemeDbApp1.Utility
{
    public interface IPublicImageSaver
    {
        /// <summary>
        /// Saves image to a temporary location for apps to use it.
        /// </summary>
        /// <param name="path">Fully-qualified path to the image.</param>
        /// <param name="tempName">Temporary name to store the image with.</param>
        /// <param name="mimeType">MIME type for the image.</param>
        /// <exception cref="ArgumentNullException">Thrown if path, tempName, or mimeType are null.</exception>
        /// <exception cref="ArgumentException">Thrown if path, tempName, or mimeType are blank or invalid. Also thrown if mimeType is not an image type.</exception>
        /// <exception cref="System.IO.FileNotFoundException">Thrown if there is no file found at the given path.</exception>
        void SaveToPublicDirectory(string path, string tempName, string mimeType);

        /// <summary>
        /// Deletes the image with the given temporary name from the temporary storage location.
        /// If there is no image with the given name and extension, this will execute as normal (no exception thrown).
        /// </summary>
        /// <param name="tempName">Temporary name that the image was previously stored with.</param>
        /// <param name="extension">Extension that the image was previously stored with. Do not include the leading period.</param>
        /// <exception cref="ArgumentNullException">Thrown if tempName or extension are null.</exception>
        /// <exception cref="ArgumentException">Thrown if tempName or extension are blank or invalid.</exception>
        void RemoveFromPublicDirectory(string tempName, string extension);
    }
}
