﻿using System.Text;

namespace MemeDbApp1.Utility
{
    /// <summary>
    /// Provides static functions for easily creating Prism navigation paths, for use with NavigationService.NavigateAsync.
    /// </summary>
    public static class NavPath
    {
        private const string DELIMITER = @"/";

        /// <summary>
        /// Creates a relative path with the given terms. Terms will be added to the path in the order that they are provided.
        /// </summary>
        /// <param name="terms">Terms to put in the path.</param>
        /// <returns>A path that can be passed to NavigationService.NavigateAsync.</returns>
        public static string Get(params string[] terms)
        {
            return Get(false, terms);
        }

        /// <summary>
        /// Creates a path with the given terms. Terms will be added to the path in the order that they are provided.
        /// </summary>
        /// <param name="isAbsolute">Boolean that is true if the path should be absolute, or false if it should be relative.</param>
        /// <param name="terms">Terms to put in the path.</param>
        /// <returns>A path that can be passed to NavigationService.NavigateAsync.</returns>
        public static string Get(bool isAbsolute = false, params string[] terms)
        {
            if (terms.Length == 0)
                { return null; }

            StringBuilder pathBuilder = new StringBuilder();
            
            if (isAbsolute)
                { pathBuilder.Append(DELIMITER); }
            
            foreach (string term in terms)
            {
                pathBuilder.Append(term);
                pathBuilder.Append(DELIMITER);
            }

            return pathBuilder.ToString();
        }
    }
}
