﻿using MemeDbApp1.Models;
using Xamarin.Forms;

namespace MemeDbApp1.Utility.Selectors
{
    public class CategoryDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate WithoutParentTemplate { get; set; }
        public DataTemplate WithParentTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            Category category = item as Category;

            if (category.Parent != null)
                { return this.WithParentTemplate; }
            else
                { return this.WithoutParentTemplate; }
        }
    }
}
