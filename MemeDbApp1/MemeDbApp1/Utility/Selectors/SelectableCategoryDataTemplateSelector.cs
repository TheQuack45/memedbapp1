﻿using MemeDbApp1.ViewModels;
using Xamarin.Forms;

namespace MemeDbApp1.Utility.Selectors
{
    public class SelectableCategoryDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate WithoutParentTemplate { get; set; }
        public DataTemplate WithParentTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var category = item as SelectableCategory;

            if (category.Parent != null)
                { return this.WithParentTemplate; }
            else
                { return this.WithoutParentTemplate; }
        }
    }
}
