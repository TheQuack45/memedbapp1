﻿namespace MemeDbApp1.Utility
{
    public static class FsPaths
    {
        public static string MainInternalDirectory { get; set; }
        public static string MainDatabasePath { get; set; }

        public static string ImagesDirectory { get; set; }

        public static string PublicImagesDirectory { get; set; }
    }
}
