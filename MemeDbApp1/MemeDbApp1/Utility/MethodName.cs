﻿using System.Runtime.CompilerServices;

namespace MemeDbApp1.Utility
{
    public static class MethodName
    {
        /// <summary>
        /// Returns the name of the method that calls this, NOT including the containing class.
        /// </summary>
        /// <param name="caller">Do not assign a value.</param>
        /// <returns>Name of the calling method.</returns>
        public static string Get([CallerMemberName] string caller = null)
        {
            return caller;
        }
    }
}
