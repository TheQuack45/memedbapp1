﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EfCoreMigrations
{
    public class InternalDbContext : DbContext
    {
        private string _databaseFilePath;

        public DbSet<Meme> Meme { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<MemeCategory> MemeCategory { get; set; }

        public InternalDbContext()
        {
            // NOTE: This must be set for EF Core to generate the migrations properly, but it doesn't need to (and should not) be set to a valid path.
            this._databaseFilePath = "";
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MemeCategory>()
                        .HasKey(mc => new { mc.MemeId, mc.CategoryId });
            modelBuilder.Entity<MemeCategory>()
                        .HasOne(mc => mc.Meme)
                        .WithMany(meme => meme.MemeCategory)
                        .HasForeignKey(mc => mc.MemeId);
            modelBuilder.Entity<MemeCategory>()
                        .HasOne(mc => mc.Category)
                        .WithMany(c => c.MemeCategory)
                        .HasForeignKey(mc => mc.CategoryId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (this._databaseFilePath == null)
            {
                throw new InvalidOperationException("This database must have a valid path set.");
            }

            optionsBuilder.UseSqlite($"Data Source={this._databaseFilePath}");
        }
    }
}
