﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EfCoreMigrations
{
    [Table(nameof(MemeCategory))]
    public class MemeCategory
    {
        [ForeignKey(nameof(EfCoreMigrations.Meme))]
        public Guid MemeId { get; set; }
        public Meme Meme { get; set; }

        [ForeignKey(nameof(EfCoreMigrations.Category))]
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
    }

}
