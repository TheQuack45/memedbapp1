﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EfCoreMigrations
{
    [Table(nameof(Meme))]
    public class Meme
    {
        public Guid Id { get; set; }

        public string Caption { get; set; }
        public string MimeType { get; set; }
        public DateTime? DateAdded { get; set; }

        public ICollection<MemeCategory> MemeCategory { get; set; } = new List<MemeCategory>();
    }

}
