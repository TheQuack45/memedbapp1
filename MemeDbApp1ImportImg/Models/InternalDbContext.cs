﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MemeDbApp1ImportImg.Models
{
    public class InternalDbContext : DbContext
    {
        private string _databaseFilePath;

        public DbSet<Image> Image { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<ImageCategory> ImageCategories { get; set; }

        public InternalDbContext(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            else if (path == String.Empty)
            {
                throw new ArgumentException("The given path must not be empty.", nameof(path));
            }

            this._databaseFilePath = path;
            //Database.EnsureDeleted();
            Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ImageCategory>()
                        .HasKey(ic => new { ic.ImageId, ic.CategoryId });
            modelBuilder.Entity<ImageCategory>()
                        .HasOne(ic => ic.Image)
                        .WithMany(image => image.ImageCategories)
                        .HasForeignKey(ic => ic.ImageId);
            modelBuilder.Entity<ImageCategory>()
                        .HasOne(ic => ic.Category)
                        .WithMany(c => c.ImageCategories)
                        .HasForeignKey(ic => ic.CategoryId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (this._databaseFilePath == null)
            {
                throw new InvalidOperationException("This database must have a valid path set.");
            }

            optionsBuilder.UseSqlite($"Data Source={this._databaseFilePath}");
        }
    }

    public class Image
    {
        public Guid Id { get; set; }

        public string Caption { get; set; }
        public string MimeType { get; set; }
        // This is nullable, because SQLite doesn't understand how to assign a default value of "literally right now". But it should never be left unset.
        public DateTime? DateAdded { get; set; }

        public ICollection<ImageCategory> ImageCategories { get; set; } = new List<ImageCategory>();
    }

    public class Category
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid? ParentId { get; set; }
        public Category Parent { get; set; }
        public ICollection<Category> Children { get; set; } = new List<Category>();

        public ICollection<ImageCategory> ImageCategories { get; set; } = new List<ImageCategory>();
    }

    public class ImageCategory
    {
        public Guid ImageId { get; set; }
        public Image Image { get; set; }

        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
