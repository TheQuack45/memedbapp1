﻿using System;
using System.Diagnostics;
using Foundation;
using MemeDbApp1ImportImg.Models;
using MobileCoreServices;
using Social;
using UIKit;

namespace MemeDbApp1ImportImg
{
    public partial class ShareViewController : SLComposeServiceViewController
    {
        public ShareViewController(IntPtr handle) : base(handle)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Do any additional setup after loading the view.
        }

        public override bool IsContentValid()
        {
            // Do validation of contentText and/or NSExtensionContext attachments here
            return true;
        }

        public override void DidSelectPost()
        {
            // TODO: This entire thing does not work and I don't know why.

            // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
            NSExtensionItem imageItem = ExtensionContext.InputItems[0];
            if (imageItem == null)
            {
                ExtensionContext.CompleteRequest(new NSExtensionItem[0], null);
                return;
            }

            NSItemProvider imageProvider = imageItem.Attachments[0];
            if (imageProvider == null)
            {
                ExtensionContext.CompleteRequest(new NSExtensionItem[0], null);
                return;
            }

            if (!imageProvider.HasItemConformingTo(UTType.Image))
            {
                // There was no image provided when sharing.
                ExtensionContext.CompleteRequest(new NSExtensionItem[0], null);
                return;
            }

            Console.WriteLine("GAGKHJEJHGE");

            imageProvider.LoadItem(UTType.Image, null, (NSObject imageObject, NSError error) =>
            {
                UIImage image;

                if (imageObject is NSUrl)
                {
                    Debug.WriteLine("imageObject is NSUrl");
                    image = UIImage.LoadFromData(NSData.FromUrl(imageObject as NSUrl));
                }
                else if (imageObject is UIImage)
                {
                    Debug.WriteLine("imageObject is UIImage");
                    image = imageObject as UIImage;
                }
                else
                {
                    // The data passed cannot be processed.
                    ExtensionContext.CompleteRequest(new NSExtensionItem[0], null);
                    return;
                }

                string caption = this.ContentText;
                if (string.IsNullOrWhiteSpace(caption))
                { caption = null; }
                Debug.WriteLine($"caption: {caption}");

                this.SaveImage(image, caption);
            });

            // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
            ExtensionContext.CompleteRequest(new NSExtensionItem[0], null);
        }

        private void SaveImage(UIImage image, string caption)
        {
            Debug.WriteLine("AGEGEGE Starting to save image");

            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string imagesPath = System.IO.Path.Combine(documentsPath, "Images");
            string libraryPath = System.IO.Path.Combine(documentsPath, "..", "Library");
            string mainDatabasePath = System.IO.Path.Combine(libraryPath, "Data.db");

            Guid imageId = Guid.NewGuid();
            string imagePath = System.IO.Path.Combine(imagesPath, (imageId.ToString() + ".png"));

            Debug.WriteLine($"Image name: {imageId.ToString() + ".png"}");
            // Save image to filesystem.
            image.AsPNG().Save(imagePath, false);

            // Add image to database.
            string mimeType = "image/png";
            this.AddImageToDatabase(mainDatabasePath, imageId, mimeType, caption);
        }

        private void AddImageToDatabase(string databasePath, Guid id, string mimeType, string caption)
        {
            var meme = new Models.Image()
            {
                Id = id,
                MimeType = mimeType,
                Caption = caption,
                DateAdded = DateTime.UtcNow,
            };

            using (var dbContext = new InternalDbContext(databasePath))
            {
                dbContext.Image.Add(meme);

                dbContext.SaveChanges();
            }
        }

        public override SLComposeSheetConfigurationItem[] GetConfigurationItems()
        {
            // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
            return new SLComposeSheetConfigurationItem[0];
        }
    }
}
